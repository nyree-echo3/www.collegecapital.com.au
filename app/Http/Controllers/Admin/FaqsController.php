<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Module;
use App\Faq;
use App\FaqCategory;
use App\Helpers\General;
use App\SpecialUrl;

use Illuminate\Http\Request;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class FaqsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
		
		$module_details = Module::where('slug', '=', 'faqs')->first();            
		view()->share('display_name', $module_details->display_name);
    }

    public function index(Request $request)
    {
        $is_filtered = $this->isFiltered($request);
        $paginate_count = session()->get('pagination-count');

        if ($is_filtered) {
            $faqs = Faq::Filter()->sortable()->orderBy('position', 'desc')->paginate($paginate_count);
        } else {
            $faqs = Faq::with('category')->sortable()->orderBy('position', 'desc')->paginate($paginate_count);
        }

        $session = session()->get('faqs-filter');
        $categories = FaqCategory::orderBy('created_at', 'desc')->get();
        return view('admin/faqs/faqs', array(
            'faqs' => $faqs,
            'categories' => $categories,
            'is_filtered' => $is_filtered,
            'session' => $session
        ));
    }

    public function add()
    {
        $categories = FaqCategory::orderBy('created_at', 'desc')->get();
        return view('admin/faqs/add', array(
            'categories' => $categories
        ));
    }

    public function edit($faq_id)
    {
        $faq = Faq::where('id', '=', $faq_id)->first();
        $categories = FaqCategory::orderBy('created_at', 'desc')->get();
        return view('admin/faqs/edit', array(
            'faq' => $faq,
            'categories' => $categories
        ));
    }

	public function preview($faq_id)
    {
		$faq = Faq::with("category")->where('id', '=', $faq_id)->first();		
		
		$general = new General();
		$view = $general->faqPreview($faq->category->slug, $faq->slug);	
		
        return ($view);
    }
	
    public function store(Request $request)
    {
        $rules = array(
            'category_id' => 'required',
            'title' => 'required',
            'description' => 'required'
        );

        $messages = [
            'category_id.required' => 'Please select category',
            'title.required' => 'Please enter title',
            'description.required' => 'Please enter desciption'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/faqs/add')->withErrors($validator)->withInput();
        }

        $faq = new Faq();
        $faq->category_id = $request->category_id;
        $faq->title = $request->title;
        $faq->description = $request->description;

        if($request->live=='on'){
           $faq->status = 'active'; 
        }

        $faq->save();
   
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/faqs/' . $faq->id . '/edit')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/faqs/')->with('message', Array('text' => 'Item has been added', 'status' => 'success'));
		}		        


    }

    public function update(Request $request)
    {
        $rules = array(
            'category_id' => 'required',
            'title' => 'required',
            'description' => 'required'
        );

        $messages = [
            'category_id.required' => 'Please select category',
            'title.required' => 'Please enter title',
            'description.required' => 'Please enter desciption'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/faqs/' . $request->id . '/edit')->withErrors($validator)->withInput();
        }

        $faq = Faq::where('id','=',$request->id)->first();
        $faq->category_id = $request->category_id;
        $faq->title = $request->title;
        $faq->description = $request->description;
		if($request->live=='on'){
           $faq->status = 'active'; 
		} else {
			$faq->status = 'passive';
        }
        $faq->save();
    
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/faqs/' . $faq->id . '/edit')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/faqs/')->with('message', Array('text' => 'Item has been updated', 'status' => 'success'));
		}
    }

    public function delete($faq_id)
    {
        $faq = Faq::where('id','=',$faq_id)->first();
        $faq->is_deleted = true;
        $faq->save();

        return \Redirect::back()->with('message', Array('text' => 'Item has been deleted.', 'status' => 'success'));
    }

    public function changeFaqStatus(Request $request, $faq_id)
    {
        $faq = Faq::where('id', '=', $faq_id)->first();
        if ($request->status == "true") {
            $faq->status = 'active';
        } else if ($request->status == "false") {
            $faq->status = 'passive';
        }
        $faq->save();

        return Response::json(['status' => 'success']);
    }

    public function sort()
    {
        $faqs = Faq::where('status','=','active')->orderBy('position', 'desc')->get();

        return view('admin/faqs/sort', array(
            'faqs' => $faqs
        ));
    }

    public function categories()
    {
        $categories = FaqCategory::orderBy('position', 'desc')->get();
        return view('admin/faqs/categories', compact('categories'));
    }

    public function addCategory()
    {
        return view('admin/faqs/add-category');
    }

    public function storeCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'slug' => 'required|unique_store:faq_categories',
            'special_url' => 'nullable|regex:/^[\w-]*$/|special_url_store',
        );

        $messages = [
            'name.required' => 'Please enter category name.',
            'slug.unique_store' => 'Seo name is in use',
            'special_url.regex' => 'Only alphanumeric characters, dashes and underscores allowed',
            'special_url.special_url_store' => 'The url is already taken'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/faqs/add-category')->withErrors($validator)->withInput();
        }

        $category = new FaqCategory();
        $category->name = $request->name;
        $category->slug = $request->slug;
        if($request->live=='on'){
            $category->status = 'active';
        }
        $category->save();

        ///////////special URL//
        if($request->special_url!=""){
            $new_special_url = new SpecialUrl();
            $new_special_url->item_id = $category->id;
            $new_special_url->module = 'faq';
            $new_special_url->type = 'category';
            $new_special_url->url = $request->special_url;
            $new_special_url->save();

        }
        ////////////////////////
        
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/faqs/' . $category->id . '/edit-category')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/faqs/categories')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
		}

    }

    public function editCategory($category_id)
    {
        $category = FaqCategory::where('id', '=', $category_id)->first();

        $category->special_url = "";
        $special_url = SpecialUrl::where('item_id','=',$category->id)->where('module','=','faq')->where('type','=','category')->first();
        if($special_url){
            $category->special_url = $special_url->url;
        }

        return view('admin/faqs/edit-category', array(
            'category' => $category,
        ));
    }

    public function updateCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'slug' => 'required|unique_update:faq_categories,'.$request->id,
            'special_url' => 'nullable|regex:/^[\w-]*$/|special_url_update:faq,category,' . $request->id,
        );

        $messages = [
            'name.required' => 'Please enter category name.',
            'slug.unique_update' => 'Seo name is in use',
            'special_url.regex' => 'Only alphanumeric characters, dashes and underscores allowed',
            'special_url.special_url_update' => 'The url is already taken'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/faqs/' . $request->id . '/edit-category')->withErrors($validator)->withInput();
        }

        $category = FaqCategory::findOrFail($request->id);
        $category->name = $request->name;
        $category->slug = $request->slug;
        if($request->live=='on'){
           $category->status = 'active'; 
		} else {
			$category->status = 'passive';
        }
        $category->save();

        ///////////special URL//
        $special_url = SpecialUrl::where('item_id', '=', $category->id)->where('module', '=', 'faq')->where('type', '=', 'category')->first();

        if ($special_url) {

            if ($request->special_url != "") {
                $special_url->url = $request->special_url;
                $special_url->save();
            } else {
                $special_url->delete();
            }
        } else {

            if ($request->special_url) {

                $new_special_url = new SpecialUrl();
                $new_special_url->item_id = $category->id;
                $new_special_url->module = 'faq';
                $new_special_url->type = 'category';
                $new_special_url->url = $request->special_url;
                $new_special_url->save();
            }
        }
        ////////////////////////

		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/faqs/' . $category->id . '/edit-category')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/faqs/categories')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
		}
    }

    public function deleteCategory($category_id)
    {
        $category = FaqCategory::where('id','=',$category_id)->first();

        if(count($category->faqs)){
            return \Redirect::to('dreamcms/faqs/categories')->with('message', Array('text' => 'Category has FAQs. Please delete FAQs first.', 'status' => 'error'));
        }

        $category->is_deleted = true;
        $category->save();

        SpecialUrl::where('item_id','=',$category->id)->where('module','=','faq')->where('type','=','category')->delete();

        return \Redirect::to('dreamcms/faqs/categories')->with('message', Array('text' => 'Category has been deleted.', 'status' => 'success'));
    }

    public function changeCategoryStatus(Request $request, $faq_category_id)
    {
        $faq_category = FaqCategory::where('id', '=', $faq_category_id)->first();
        if ($request->status == "true") {
            $faq_category->status = 'active';
        } else if ($request->status == "false") {
            $faq_category->status = 'passive';
        }
        $faq_category->save();

        return Response::json(['status' => 'success']);
    }

    public function sortCategory()
    {
        $categories = FaqCategory::where('status','=','active')->orderBy('position', 'desc')->get();

        return view('admin/faqs/sort-category', array(
            'categories' => $categories
        ));
    }

    public function emptyFilter()
    {
        session()->forget('faqs-filter');
        return redirect()->to('dreamcms/faqs');
    }

    public function isFiltered($request)
    {

        $filter_control = false;

        if ($request->category && $request->category != "all") {
            $filter_control = true;
        }

        if ($request->search) {
            $filter_control = true;
        }

        if ($filter_control) {
            $request->session()->put('faqs-filter', [
                'category' => $request->category,
                'search' => $request->search
            ]);
        }

        if (session()->has('faqs-filter')) {
            $filter_control = true;
        }

        return $filter_control;
    }

}