<?php

namespace App\Http\Controllers\Site;

use App\Helpers\NavigationBuilder;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Link;
use App\LinkCategory;

class LinksController extends Controller
{    	
	public function getLinks($category_id = "", $limit = 50){
		if ($category_id == "")  {
			$links = Link::where('status', '=', 'active')						
						->orderBy('position', 'desc')
				        ->paginate($limit);	
		} else {
		   $links = Link::where('status', '=', 'active')
			            ->where('category_id', '=', $category_id)						
						->orderBy('position', 'desc')
			            ->paginate($limit);	
		}		
		return($links);
	}		  	
}
