<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\TeamMember;
use App\TeamCategory;

class TeamController extends Controller
{
    public function index(Request $request)
    {

        $categories = TeamCategory::where('status','=','active')->orderBy('position', 'desc')->get();

		$items = TeamMember::where('status','=', 'active')->orderBy('position', 'desc')->get();

		$meta_title_inner = "Our People";
		$meta_keywords_inner = "Our People";
		$meta_description_inner = "Our People";

        return view('site/team/list', array(
            'categories' => $categories,
            'category' => "Our People",
            'items' => $items,
            'meta_title_inner' => $meta_title_inner,
            'meta_keywords_inner' => $meta_keywords_inner,
            'meta_description_inner' => $meta_description_inner
        ));

    }

    public function member($category, $team_member)
    {
        $categories = TeamCategory::where('status','=','active')->orderBy('position', 'desc')->get();
        $category = TeamCategory::where('slug','=',$category)->first();
        $member = TeamMember::where('slug','=',$team_member)->first();

        return view('site/team/item', array(
            'categories' => $categories,
            'category' => $category,
            'team_member' => $member,
            'meta_title_inner' => $member->name.'- Team',
            'meta_keywords_inner' => $member->name,
            'meta_description_inner' => $member->name
        ));
    }
	
	public function getTeam($category_id = "", $limit = 50){
		if ($category_id == "")  {
			$team = TeamMember::where('status', '=', 'active')						
						->orderBy('position', 'desc')
						->paginate($limit);	
		} else {
		   $team = TeamMember::where('status', '=', 'active')
			            ->where('category_id', '=', $category_id)						
						->orderBy('position', 'desc')
						->paginate($limit);		
		}
		
		return($team);
	}
}
