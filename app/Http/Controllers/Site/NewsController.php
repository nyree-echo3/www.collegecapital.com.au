<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\News;
use App\NewsCategory;

class NewsController extends Controller
{
    public function list($category_slug = "", $item_slug = ""){
    	$side_nav = $this->getCategories();

		if ($category_slug == "")  {
		   // Get Latest News
		   $category_name = "Latest News";	
			
		   $items = $this->getNews();

		} elseif ($category_slug != "" && $item_slug == "") {
		  // Get Category News	
		  $category = $this->getCategory($category_slug);
		  $category_name = $category->name;	
			
		  $items = $this->getNews($category->id);
		}

		return view('site/news/list', array(            			
			'side_nav' => $side_nav,
			'category_name' => $category_name,
			'items' => $items,			
        ));

    }
	
    public function item($category_slug, $item_slug, $mode = ""){
    	$side_nav = $this->getCategories();				  			
		$news_item = $this->getNewsItem($item_slug, $mode);			  
			
		return view('site/news/item', array(            			
			'side_nav' => $side_nav,					
			'news_item' => $news_item,	
			'mode' => $mode,
        ));
    }	
	
	public function archive(Request $request, $age = ""){        		
    	//$side_nav = $this->getCategories();	
				
		// Get Archived News		
		$items = $this->getNewsArchive($age);			
		
		return view('site/news/archive', array(            								
			'age' => $age,	
			'items' => $items,			
        ));

    }
	
	public function getCategories(){
		$categories = NewsCategory::whereHas("news")->where('status', '=', 'active')->get();
		foreach ($categories as $category){
            $category->url = $category->url;
        }
		return($categories);
	}	
	
	public function getNews($category_id = "", $limit = 2){
		$today = date("Y/m/d");		
		
		if ($category_id == "")  {
			$news = News::where('status', '=', 'active')
						->where('start_date', '<=', $today)
						->where('archive_date', '>', $today)
						->orderBy('start_date', 'desc')
						->paginate($limit);	
		} else {
		   $news = News::where('status', '=', 'active')
			            ->where('category_id', '=', $category_id)
						->where('start_date', '<=', $today)
						->where('archive_date', '>', $today)
						->orderBy('start_date', 'desc')
						->paginate($limit);		
		}
		
		return($news);
	}
	
	public function getNewsArchive($age = 6){
		$today = date("Y/m/d");		
		
		switch ($age)  {
			case 0:
		        $start_period = $today;
		        $end_period = date("Y-m-d", strtotime($today . ' -6 months'));
				break;
			
			case 6:
		        $start_period = date("Y-m-d", strtotime($today  . ' -6 months'));
		        $end_period = date("Y-m-d", strtotime($start_period . ' -6 months'));
				break;
				
			case 12:
		        $start_period = date("Y-m-d", strtotime($today  . ' -12 months'));
		        $end_period = date("Y-m-d", strtotime($start_period . ' -12 months'));
				break;
			
			case 24:
		        $start_period = date("Y-m-d", strtotime($today  . ' -24 months'));
		        $end_period = date("Y-m-d", strtotime($start_period . ' -12 months'));
				break;
			
			case 36:
		        $start_period = date("Y-m-d", strtotime($today  . ' -36 months'));
		        $end_period = date("Y-m-d", strtotime($start_period . ' -100 years'));
				break;
				
		}				
		
		$news = News::whereHas("categoryactive")
			        ->where('status', '=', 'active')						
					->where('archive_date', '<=', $start_period)
			        ->where('archive_date', '>', $end_period)
			        ->where('status', '=', 'active')
					->orderBy('start_date', 'desc')
					->paginate(2);
		;
		
		return($news);
	}
	  
	public function getNewsItem($item_slug, $mode){		
		if ($mode == "preview") {
		   $news = News::where(['slug' => $item_slug])->first();							
		} else {
		   $news = News::where(['status' => 'active', 'slug' => $item_slug])->first();						
		}
		return($news);
	}
	
	public function getCategory($category_slug){

		$categories = NewsCategory::where('slug', '=', $category_slug)->first();		
		return($categories);
	}		
}
