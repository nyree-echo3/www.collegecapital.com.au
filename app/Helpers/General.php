<?php
namespace App\Helpers;

use App\Module;
use App\Page;
use App\PageCategory;
use App\NewsCategory;
use App\GalleryCategory;
use App\FaqCategory;
use App\DocumentCategory;
use App\ProjectCategory;
use App\ProductCategory;
use App\TeamCategory;

use App\Http\Controllers\Site\PagesController;
use App\Http\Controllers\Site\NewsController;
use App\Http\Controllers\Site\GalleryController;
use App\Http\Controllers\Site\FaqsController;
use App\Http\Controllers\Site\DocumentsController;
use App\Http\Controllers\Site\ProjectsController;
use App\Http\Controllers\Site\ProductsController;
use App\Http\Controllers\Site\ClaimsController;
use App\Http\Controllers\Site\TeamController;
use App\Http\Controllers\Site\TestimonialsController;
use App\Http\Controllers\Site\LinksController;

class General {

	public function getNavigation($sub_level = false){
        $page_categories = PageCategory::whereHas("pages")->with("pages")->orderBy('position', 'desc')->where('top_menu', 1)->get();
		$modules = Module::where('status', '=', 'active')->where('top_menu', '=', 'active')->where('name', '<>', 'Pages')->where('name', '<>', 'Settings')->orderBy('position', 'asc')->get();

		foreach ($modules as $module){
		    $module->url = $module->slug;
        }

        $modules = $modules->toArray();

        $arrSize = sizeof($modules);
		// Add CMS Modules and Page Categories into one array for listing
		foreach ($page_categories as $row):

		     $modules[$arrSize]['id'] = $row->id;
			 $modules[$arrSize]['position'] = $row->position;
			 $modules[$arrSize]['name'] = "Pages";
			 $modules[$arrSize]['display_name'] = $row->name;
			 $modules[$arrSize]['top_menu'] = $row->top_menu;
		     $modules[$arrSize]['slug'] = $row->slug;
		     $modules[$arrSize]['url'] = $row->url;

		     if (isset($row->pages))  {

		         foreach($row->pages as $page){
                     $page->url = $page->url;
                 }

				 $nav_sub = $this->filter_by_value($row->pages->toArray(), 'parent_page_id', '');
				 $nav_sub_sub = $this->filter_by_not_value($row->pages->toArray(), 'parent_page_id', '');
				
		        $modules[$arrSize]['nav_sub'] = $nav_sub;	
				$modules[$arrSize]['nav_sub_sub'] = $nav_sub_sub;
			 }
		
			 $arrSize++;
		endforeach;

		// Get Sub Navigation
		if ($sub_level == true)  {
		    $modules = $this->getNavigationSub($modules);						
		}
		
		usort($modules, function($a, $b) {
			return $a['position'] <=> $b['position'];
		});

		//dd($modules);

        return ($modules);
	}
	
	private function getNavigationSub($navigation)  {		
		foreach ($navigation as $key => $value):		 		
		   switch ($value["name"])  {				   
			   case "News":
			      $news = new NewsController();
			      $nav_sub = $news->getCategories();
			      $navigation[$key]['nav_sub'] = $nav_sub;
				  break;
				   
			   case "Gallery":
			      $gallery = new GalleryController();
			      $nav_sub = $gallery->getCategories(); 		  
			      $navigation[$key]['nav_sub'] = $nav_sub;
				  break;	 
				   
			   case "FAQS":
				  $faqs = new FaqsController();
			      $nav_sub = $faqs->getCategories(); 			       
			      $navigation[$key]['nav_sub'] = $nav_sub;
				  break;	
				   
			   case "Document":
				  $document = new DocumentsController();
			      $nav_sub = $document->getCategories(); 				   			    
			      $navigation[$key]['nav_sub'] = $nav_sub;
				  break;	
				   
			   case "Project":
				  $project = new ProjectsController();
			      $nav_sub = $project->getCategories(); 				   			    
			      $navigation[$key]['nav_sub'] = $nav_sub;
				  break;	
				   
			   case "Product":
				  $product = new ProductsController();
			      $nav_sub = $product->getCategories(); 				   			    
			      $navigation[$key]['nav_sub'] = $nav_sub;
				  break;	
		   }			 

		endforeach;				

		return ($navigation);
	}	
	
	public function getQuickClaims()  {
		$claims = new ClaimsController();
	    $home_quick_claims = $claims->quickclaims(); 
		
		return ($home_quick_claims);
	}
	
	public function getHomeNews()  {
		$news = new NewsController();
	    $home_news = $news->getNews("", 3); 
		
		return ($home_news);
	}
	
	public function getHomeProjects()  {
		$projects = new ProjectsController();
	    $home_projects = $projects->getProjects("", 3); 
		
		return ($home_projects);
	}
	
	public function getHomeProducts()  {
		$products = new ProductsController();
	    $home_products = $products->getProducts("", 3); 
		
		return ($home_products);
	}
	
	public function getHomeTeam()  {
		$team = new TeamController();
	    $home_team = $team->getTeam(""); 
		
		return ($home_team);
	}
	
	public function getHomeLinks()  {
		$links = new LinksController();
	    $home_links = $links->getLinks(""); 
		
		return ($home_links);
	}		
	
	public function getHomeTestimonials()  {
		$testimonials = new TestimonialsController();
	    $home_testimonials = $testimonials->getItems(""); 
		
		return ($home_testimonials);
	}
	
	public function pagePreview($category_slug, $page_slug)  {
		$pages = new PagesController();
	    $view = $pages->index($category_slug, $page_slug, "preview"); 
		return ($view);
	}
	
	public function newsPreview($category_slug, $page_slug)  {
		$news = new NewsController();
	    $view = $news->item($category_slug, $page_slug, "preview"); 
		return ($view);
	}
	
	public function faqPreview($category_slug)  {
		$faqs = new FaqsController();
	    $view = $faqs->index($category_slug, "preview"); 
		return ($view);
	}
	
	public function documentPreview($category_slug, $page_slug)  {
		$documents = new DocumentsController();
	    $view = $documents->index($category_slug, $page_slug, "preview"); 
		return ($view);
	}
	
	public function productPreview($category_slug, $page_slug)  {
		$products = new ProductsController();		
	    $view = $products->item($category_slug, $page_slug, "preview"); 
		return ($view);
	}
	
	private function filter_by_value ($array, $index, $value){
		$newarray = array();
		
        if(is_array($array) && count($array)>0) 
        {
            foreach(array_keys($array) as $key){
                $temp[$key] = $array[$key][$index];
                
                if ($temp[$key] == $value){
                    $newarray[$key] = $array[$key];
                }
            }
          }
      return $newarray;
    }
	
	private function filter_by_not_value ($array, $index, $value){
		$newarray = array();
		
        if(is_array($array) && count($array)>0) 
        {
            foreach(array_keys($array) as $key){
                $temp[$key] = $array[$key][$index];
                
                if ($temp[$key] != $value){
                    $newarray[$key] = $array[$key];
                }
            }
          }
      return $newarray;
    }
}
?>