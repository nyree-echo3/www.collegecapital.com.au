<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Module;
use App\Permission;
use App\Role;
use Illuminate\Support\Facades\Cache;

class SetPermissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'set:permissions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sets permissions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Cache::forget('spatie.permission.cache');

        Role::create(['name' => 'admin']);		
        Permission::create(['name' => 'view-user', 'translation' => 'View User', 'order_id' => 1, 'module_id' => 0]);
        Permission::create(['name' => 'add-user', 'translation' => 'Add User', 'order_id' => 2, 'module_id' => 0]);
        Permission::create(['name' => 'edit-user', 'translation' => 'Edit User', 'order_id' => 3, 'module_id' => 0]);
        Permission::create(['name' => 'delete-user', 'translation' => 'Delete User', 'order_id' => 4, 'module_id' => 0]);
        Permission::create(['name' => 'edit-permission', 'translation' => 'Edit User Permissions', 'order_id' => 5, 'module_id' => 0]);
        Permission::create(['name' => 'user-change-password', 'translation' => 'Change User Password', 'order_id' => 6, 'module_id' => 0]);

        //Settings Module Permissions
        Permission::create(['name' => 'view-settings', 'translation' => 'View Settings', 'order_id' => 1, 'module_id' => -1]);
        Permission::create(['name' => 'edit-general', 'translation' => 'Edit General Settings', 'order_id' => 2, 'module_id' => -1]);
        Permission::create(['name' => 'edit-home-page', 'translation' => 'Edit Home Page', 'order_id' => 3, 'module_id' => -1]);
        Permission::create(['name' => 'edit-header-images', 'translation' => 'Edit Header Images', 'order_id' => 4, 'module_id' => -1]);
        Permission::create(['name' => 'edit-contact-details', 'translation' => 'Edit Contact Details', 'order_id' => 5, 'module_id' => -1]);
        Permission::create(['name' => 'edit-navigation', 'translation' => 'Edit Navigation', 'order_id' => 6, 'module_id' => -1]);
        Permission::create(['name' => 'edit-home-page-slider', 'translation' => 'Edit Home Page Slider', 'order_id' => 7, 'module_id' => -1]);
        Permission::create(['name' => 'edit-social-media', 'translation' => 'Edit Social Media', 'order_id' => 8, 'module_id' => -1]);
        Permission::create(['name' => 'delete-home-page-slider', 'translation' => 'Delete Home Page Slider', 'order_id' => 7, 'module_id' => -1]);


        //Page Module Permissions
        $page_module = Module::where('slug', '=', 'pages')->first();
        Permission::create(['name' => 'view-pages', 'translation' => 'View Pages', 'order_id' => 1, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'add-page-category', 'translation' => 'Add Page Category', 'order_id' => 2, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'edit-page-category', 'translation' => 'Edit Page Category', 'order_id' => 3, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'delete-page-category', 'translation' => 'Delete Page Category', 'order_id' => 4, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'sort-page-category', 'translation' => 'Sort Page Category', 'order_id' => 5, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'add-page', 'translation' => 'Add Page', 'order_id' => 6, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'edit-page', 'translation' => 'Edit Page', 'order_id' => 7, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'delete-page', 'translation' => 'Delete Page', 'order_id' => 8, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'sort-page', 'translation' => 'Sort Pages', 'order_id' => 9, 'module_id' => $page_module->id]);

        //News Module Permissions
        $page_module = Module::where('slug', '=', 'news')->first();
        Permission::create(['name' => 'view-news', 'translation' => 'View News', 'order_id' => 1, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'add-news-category', 'translation' => 'Add News Category', 'order_id' => 2, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'edit-news-category', 'translation' => 'Edit News Category', 'order_id' => 3, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'delete-news-category', 'translation' => 'Delete News Category', 'order_id' => 4, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'sort-news-category', 'translation' => 'Sort News Category', 'order_id' => 5, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'add-news', 'translation' => 'Add News', 'order_id' => 6, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'edit-news', 'translation' => 'Edit News', 'order_id' => 7, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'delete-news', 'translation' => 'Delete News', 'order_id' => 8, 'module_id' => $page_module->id]);

        //FAQs Module Permissions
        $page_module = Module::where('slug', '=', 'faqs')->first();
        Permission::create(['name' => 'view-faqs', 'translation' => 'View FAQS', 'order_id' => 1, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'add-faqs-category', 'translation' => 'Add FAQS Category', 'order_id' => 2, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'edit-faqs-category', 'translation' => 'Edit FAQS Category', 'order_id' => 3, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'delete-faqs-category', 'translation' => 'Delete FAQS Category', 'order_id' => 4, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'sort-faqs-category', 'translation' => 'Sort FAQS Category', 'order_id' => 5, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'add-faqs', 'translation' => 'Add FAQS', 'order_id' => 6, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'edit-faqs', 'translation' => 'Edit FAQS', 'order_id' => 7, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'delete-faqs', 'translation' => 'Delete FAQS', 'order_id' => 8, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'sort-faq', 'translation' => 'Sort FAQS', 'order_id' => 9, 'module_id' => $page_module->id]);


        //Gallery Module Permissions
        $page_module = Module::where('slug', '=', 'gallery')->first();
        Permission::create(['name' => 'view-gallery', 'translation' => 'View Gallery', 'order_id' => 1, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'add-gallery-category', 'translation' => 'Add Gallery Category', 'order_id' => 2, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'edit-gallery-category', 'translation' => 'Edit Gallery Category', 'order_id' => 3, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'delete-gallery-category', 'translation' => 'Edit Gallery Category', 'order_id' => 4, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'sort-gallery-category', 'translation' => 'Sort Gallery Category', 'order_id' => 5, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'add-image', 'translation' => 'Add Image', 'order_id' => 6, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'edit-image', 'translation' => 'Edit Image', 'order_id' => 7, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'delete-image', 'translation' => 'Delete Image', 'order_id' => 8, 'module_id' => $page_module->id]);

        //Contact Module Permissions
        $page_module = Module::where('slug', '=', 'contact')->first();
        Permission::create(['name' => 'view-contact', 'translation' => 'View Contact', 'order_id' => 1, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'view-edit-details', 'translation' => 'View / Edit Details', 'order_id' => 2, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'read-message', 'translation' => 'Read Message', 'order_id' => 3, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'delete-message', 'translation' => 'Delete Message', 'order_id' => 4, 'module_id' => $page_module->id]);
		
		//Claims Module Permissions
        $page_module = Module::where('slug', '=', 'claims')->first();
        Permission::create(['name' => 'view-claims', 'translation' => 'View Claims', 'order_id' => 1, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'view-edit-claims', 'translation' => 'View / Edit Claims', 'order_id' => 2, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'read-claims', 'translation' => 'Read Claims', 'order_id' => 3, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'delete-claims', 'translation' => 'Delete Claims', 'order_id' => 4, 'module_id' => $page_module->id]);

        //Members Module Permissions
        $page_module = Module::where('slug', '=', 'members')->first();
        Permission::create(['name' => 'view-members', 'translation' => 'View Members', 'order_id' => 1, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'add-members-type', 'translation' => 'Add Members Type', 'order_id' => 2, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'edit-members-type', 'translation' => 'Edit Members Type', 'order_id' => 3, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'delete-members-type', 'translation' => 'Delete Members Type', 'order_id' => 4, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'sort-members-type', 'translation' => 'Sort Members Type', 'order_id' => 5, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'add-members', 'translation' => 'Add Member', 'order_id' => 6, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'edit-members', 'translation' => 'Edit Member', 'order_id' => 7, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'delete-members', 'translation' => 'Delete Member', 'order_id' => 8, 'module_id' => $page_module->id]);


		//Documents Module Permissions
        $page_module = Module::where('slug', '=', 'documents')->first();
        Permission::create(['name' => 'view-documents', 'translation' => 'View Documents', 'order_id' => 1, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'add-documents-category', 'translation' => 'Add Documents Category', 'order_id' => 2, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'edit-documents-category', 'translation' => 'Edit Documents Category', 'order_id' => 3, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'delete-documents-category', 'translation' => 'Delete Documents Category', 'order_id' => 4, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'sort-documents-category', 'translation' => 'Sort Documents Category', 'order_id' => 5, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'add-documents', 'translation' => 'Add Document', 'order_id' => 6, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'edit-documents', 'translation' => 'Edit Document', 'order_id' => 7, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'delete-documents', 'translation' => 'Delete Document', 'order_id' => 8, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'sort-documents', 'translation' => 'Sort Document', 'order_id' => 9, 'module_id' => $page_module->id]);

        //Projects Module Permissions
        $page_module = Module::where('slug', '=', 'projects')->first();
        Permission::create(['name' => 'view-projects', 'translation' => 'View Projects', 'order_id' => 1, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'add-projects-category', 'translation' => 'Add Projects Category', 'order_id' => 2, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'edit-projects-category', 'translation' => 'Edit Projects Category', 'order_id' => 3, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'delete-projects-category', 'translation' => 'Delete Projects Category', 'order_id' => 4, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'sort-projects-category', 'translation' => 'Sort Projects Category', 'order_id' => 5, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'add-projects', 'translation' => 'Add Project', 'order_id' => 6, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'edit-projects', 'translation' => 'Edit Project', 'order_id' => 7, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'delete-projects', 'translation' => 'Delete Project', 'order_id' => 8, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'sort-projects', 'translation' => 'Sort Project', 'order_id' => 9, 'module_id' => $page_module->id]);

        //Products Module Permissions
        $page_module = Module::where('slug', '=', 'products')->first();
        Permission::create(['name' => 'view-products', 'translation' => 'View Products', 'order_id' => 1, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'add-products-category', 'translation' => 'Add Products Category', 'order_id' => 2, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'edit-products-category', 'translation' => 'Edit Products Category', 'order_id' => 3, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'delete-products-category', 'translation' => 'Delete Products Category', 'order_id' => 4, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'sort-products-category', 'translation' => 'Sort Products Category', 'order_id' => 5, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'add-products', 'translation' => 'Add Product', 'order_id' => 6, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'edit-products', 'translation' => 'Edit Product', 'order_id' => 7, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'delete-products', 'translation' => 'Delete Product', 'order_id' => 8, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'sort-products', 'translation' => 'Sort Product', 'order_id' => 9, 'module_id' => $page_module->id]);

		//Orders Module Permissions
        $page_module = Module::where('slug', '=', 'orders')->first();
        Permission::create(['name' => 'view-orders', 'translation' => 'View Orders', 'order_id' => 1, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'edit-orders', 'translation' => 'Edit Order', 'order_id' => 2, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'delete-orders', 'translation' => 'Delete Order', 'order_id' => 3, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'sort-orders', 'translation' => 'Sort Order', 'order_id' => 4, 'module_id' => $page_module->id]);

        //Testimonials Module Permissions
        $page_module = Module::where('slug', '=', 'testimonials')->first();
        Permission::create(['name' => 'view-testimonials', 'translation' => 'View Testimonials', 'order_id' => 1, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'add-testimonials', 'translation' => 'Add Testimonials', 'order_id' => 6, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'edit-testimonials', 'translation' => 'Edit Testimonials', 'order_id' => 7, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'delete-testimonials', 'translation' => 'Delete Testimonials', 'order_id' => 8, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'sort-testimonial', 'translation' => 'Sort Testimonials', 'order_id' => 9, 'module_id' => $page_module->id]);

        //Team Module Permissions
        $page_module = Module::where('slug', '=', 'team')->first();
        Permission::create(['name' => 'view-team', 'translation' => 'View Team', 'order_id' => 1, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'add-team-category', 'translation' => 'Add Team Category', 'order_id' => 2, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'edit-team-category', 'translation' => 'Edit Team Category', 'order_id' => 3, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'delete-team-category', 'translation' => 'Delete Team Category', 'order_id' => 4, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'sort-team-category', 'translation' => 'Sort Team Category', 'order_id' => 5, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'add-team-member', 'translation' => 'Add Team Member', 'order_id' => 6, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'edit-team-member', 'translation' => 'Edit Team Member', 'order_id' => 7, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'delete-team-member', 'translation' => 'Delete Team Member', 'order_id' => 8, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'sort-team-member', 'translation' => 'Sort Team Member', 'order_id' => 9, 'module_id' => $page_module->id]);

        //Properties Module Permissions
        $page_module = Module::where('slug', '=', 'properties')->first();
        Permission::create(['name' => 'view-properties', 'translation' => 'View Properties', 'order_id' => 1, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'add-properties-category', 'translation' => 'Add Properties Category', 'order_id' => 2, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'edit-properties-category', 'translation' => 'Edit Properties Category', 'order_id' => 3, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'delete-properties-category', 'translation' => 'Delete Properties Category', 'order_id' => 4, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'sort-properties-category', 'translation' => 'Sort Properties Category', 'order_id' => 5, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'add-properties', 'translation' => 'Add Property', 'order_id' => 6, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'edit-properties', 'translation' => 'Edit Property', 'order_id' => 7, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'delete-properties', 'translation' => 'Delete Property', 'order_id' => 8, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'sort-properties', 'translation' => 'Sort Properties', 'order_id' => 9, 'module_id' => $page_module->id]);

        //Links Module Permissions
        $page_module = Module::where('slug', '=', 'links')->first();
        Permission::create(['name' => 'view-links', 'translation' => 'View Links', 'order_id' => 1, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'add-links-category', 'translation' => 'Add Link Category', 'order_id' => 2, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'edit-links-category', 'translation' => 'Edit Link Category', 'order_id' => 3, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'delete-links-category', 'translation' => 'Delete Link Category', 'order_id' => 4, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'sort-links-category', 'translation' => 'Sort Link Category', 'order_id' => 5, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'add-link', 'translation' => 'Add Link', 'order_id' => 6, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'edit-link', 'translation' => 'Edit Link', 'order_id' => 7, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'delete-link', 'translation' => 'Delete Link', 'order_id' => 8, 'module_id' => $page_module->id]);
        Permission::create(['name' => 'sort-link', 'translation' => 'Sort Link', 'order_id' => 9, 'module_id' => $page_module->id]);
		
        /*$user = User::where('id',1)->first();
        $user->givePermissionTo('view-user');
        $user->givePermissionTo('add-user');
		$user->givePermissionTo('edit-user');
        $user->givePermissionTo('edit-permission');*/
    }
}
