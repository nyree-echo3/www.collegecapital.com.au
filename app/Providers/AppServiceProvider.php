<?php

namespace App\Providers;

use App\Member;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use App\Module;
use App\Setting;
use App\User;
use App\PageCategory;
use Carbon\Carbon;
use App\Helpers\General;
use App\ImagesHomeSlider;
use Illuminate\Support\Facades\Route;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        ////ADMIN
        if (!session()->has('pagination-count')) {
            session(['pagination-count' => 25]);
        }

        if (!session()->has('sidebar-state')) {
            session(['sidebar-state' => '']);
        }

        Validator::extend('date_checker', function ($attribute, $value, $parameters, $validator) {
            $archive_date = Carbon::createFromFormat('d/m/Y', $value);
            $start_date = Carbon::createFromFormat('d/m/Y', $parameters[0]);

            return $archive_date->gt($start_date);
        });

        Validator::extend('unique_store', function ($attribute, $value, $parameters, $validator) {
            $table_name = $parameters[0];
            if (\DB::table($table_name)->where($attribute,'=',$value)->where('is_deleted', '=', 'false')->exists()) {
                return false;
            }
            return true;
        });

        Validator::extend('unique_update', function ($attribute, $value, $parameters, $validator) {
            $table_name = $parameters[0];
            $id = $parameters[1];
            if (\DB::table($table_name)->where($attribute,'=',$value)->where('id', '!=', $id)->where('is_deleted', '=', 'false')->exists()) {
                return false;
            }
            return true;
        });

        Validator::extend('special_url_store', function ($attribute, $value, $parameters, $validator) {

            foreach (Route::getRoutes()->getIterator() as $route) {
                if($value==$route->uri){
                    return false;
                }
            }

            if (\DB::table('special_urls')->where('url','=',$value)->exists()) {
                return false;
            }
            return true;
        });

        Validator::extend('special_url_update', function ($attribute, $value, $parameters, $validator) {

            foreach (Route::getRoutes()->getIterator() as $route) {
                if($value==$route->uri){
                    return false;
                }
            }

            $module = $parameters[0];
            $type = $parameters[1];
            $id = $parameters[2];

            $special_url = \DB::table('special_urls')->where('item_id', '=', $id)->where('module', '=', $module)->where('type', '=', $type)->first();

            if($special_url){
                if (\DB::table('special_urls')->where('id', '!=', $special_url->id)->where('url','=',$value)->exists()) {
                    return false;
                }
            }else{
                if (\DB::table('special_urls')->where('url','=',$value)->exists()) {
                    return false;
                }
            }

            return true;
        });

        view()->composer('admin/partials/header', function ($view) {

            $settings = Setting::where('key', '=', 'company-name')->first();
            $view->with('company_name', $settings->value);
        });

        view()->composer('admin/partials/menu', function ($view) {

            $pages = Module::where('slug', '=', 'pages')->first();
            $view->with('pages_status', $pages->status);
			$view->with('pages_display_name', $pages->display_name);

            $news = Module::where('slug', '=', 'news')->first();
            $view->with('news_status', $news->status);
			$view->with('news_display_name', $news->display_name);

            $gallery = Module::where('slug', '=', 'gallery')->first();
            $view->with('gallery_status', $gallery->status);
			$view->with('gallery_display_name', $gallery->display_name);

            $contact = Module::where('slug', '=', 'contact')->first();
            $view->with('contact_status', $contact->status);
			$view->with('contact_display_name', $contact->display_name);

            $faqs = Module::where('slug', '=', 'faqs')->first();
            $view->with('faqs_status', $faqs->status);
			$view->with('faqs_display_name', $faqs->display_name);

            $members = Module::where('slug', '=', 'members')->first();
            $view->with('members_status', $members->status);
			$view->with('members_display_name', $members->display_name);

            $documents = Module::where('slug', '=', 'documents')->first();
            $view->with('documents_status', $documents->status);
			$view->with('documents_display_name', $documents->display_name);
			
			$projects = Module::where('slug', '=', 'projects')->first();
            $view->with('projects_status', $projects->status);
			$view->with('projects_display_name', $projects->display_name);
			
			$products = Module::where('slug', '=', 'products')->first();
            $view->with('products_status', $products->status);
			$view->with('products_display_name', $products->display_name);          
			
			$orders = Module::where('slug', '=', 'orders')->first();
            $view->with('orders_status', $orders->status);
			$view->with('orders_display_name', $orders->display_name);

            $testimonials = Module::where('slug', '=', 'testimonials')->first();
            $view->with('testimonials_status', $testimonials->status);
			$view->with('testimonials_display_name', $testimonials->display_name);

            $team = Module::where('slug', '=', 'team')->first();
            $view->with('team_status', $team->status);						
			
			$view->with('team_display_name', $team->display_name);
			
			$links = Module::where('slug', '=', 'links')->first();
            $view->with('links_status', $links->status);
			$view->with('links_display_name', $links->display_name);

        });

        ////SITE			
        // Home Page
		view()->composer('site/index', function ($view) {

			// Company Name
            $settings = Setting::where('key', '=', 'company-name')->first();
            $view->with('company_name', $settings->value);
						
			// Intro Text
			$settings = Setting::where('key', '=', 'home-intro-text')->first();
            $view->with('home_intro_text', $settings->value);	
			
			// Quick Quote Panel
			$general = new General();
			$home_quick_claims = $general->getQuickClaims(true);
			$view->with('home_quick_claims', $home_quick_claims);	
			
			// News Panel
			$general = new General();
			$home_news = $general->getHomeNews(true);
			$view->with('home_news', $home_news);	
			
			// Project Panel
			$general = new General();
			$home_projects = $general->getHomeProjects(true);
			$view->with('home_projects', $home_projects);	
			
			// Product Panel
			$general = new General();
			$home_products = $general->getHomeProducts(true);
			$view->with('home_products', $home_products);	
			
			// Team Panel
			$general = new General();
			$home_team = $general->getHomeTeam(true);
			$view->with('home_team', $home_team);		
			
			// Testimonials Panel
			$general = new General();
			$home_testimonials = $general->getHomeTestimonials(true);
			$view->with('home_testimonials', $home_testimonials);	
			
			// Links Panel
			$general = new General();
			$home_links = $general->getHomeLinks(true);
			$view->with('home_links', $home_links);	
			
			
        });
		
		view()->composer('site/layouts/app', function ($view) {

			// Company Name
            $settings = Setting::where('key', '=', 'company-name')->first();
            $view->with('company_name', $settings->value);
			
			// Meta Title
			$settings = Setting::where('key', '=', 'meta-title')->first();
            $view->with('meta_title', $settings->value);
			
			// Meta Keywords
			$settings = Setting::where('key', '=', 'meta-keywords')->first();
            $view->with('meta_keywords', $settings->value);
			
			// Meta Description
			$settings = Setting::where('key', '=', 'meta-description')->first();
            $view->with('meta_description', $settings->value);
			
			// Intro Text
			$settings = Setting::where('key', '=', 'home-intro-text')->first();
            $view->with('home_intro_text', $settings->value);
			
			// Go Live Date
			$settings = Setting::where('key', '=', 'live-date')->first();
            $view->with('live_date', $settings->value);
			
			// Google Analytics
			$settings = Setting::where('key', '=', 'google-analytics')->first();
            $view->with('google_analytics', $settings->value);
        });
		
        //Populate Main Menu
        view()->composer('site/partials/navigation', function ($view) {
            // Navigation
			$general = new General();
			$navigation = $general->getNavigation(true);
			
			// Company Name
            $settings = Setting::where('key', '=', 'company-name')->first();
            $company_name = $settings->value;

			// Phone Number
            $settings = Setting::where('key', '=', 'phone-number')->first();
            $phone_number = $settings->value;
			
            $view->with(array(				
                'navigation' => $navigation,
				'company_name' => $company_name,
				'phone_number' => $phone_number,
            ));
        });
		
		//Populate Side Menu
        view()->composer('site/partials/sidebar-navigation', function ($view) {
            // Navigation
			$general = new General();
			$navigation = $general->getNavigation(true);						
			
            $view->with(array(				
                'navigation' => $navigation,				
            ));
        });
		
		//Populate Slider Carousel
        view()->composer('site/partials/carousel', function ($view) {            
			// Slider Images
            $images = ImagesHomeSlider::where('status', '=', 'active')->orderBy('position', 'desc')->get();            
			
            $view->with(array(
                'images' => $images,				
            ));
        });
		
		//Populate Main Menu - Secondary
        view()->composer('site/partials/index-insurance', function ($view) {
            // Navigation
			$general = new General();
			$navigation = $general->getNavigation(true);
			
            $view->with(array(				
                'navigation' => $navigation,				
            ));
        });
		
		//Populate Footer
        view()->composer('site/partials/footer', function ($view) {
            // Navigation
			$general = new General();
			$modules = $general->getNavigation();
			
			// Company Name
            $settings = Setting::where('key', '=', 'company-name')->first();
            $company_name = $settings->value;

            // Email
            $settings = Setting::where('key', '=', 'email')->first();
            $email = $settings->value;

			// Phone Number
            $settings = Setting::where('key', '=', 'phone-number')->first();
            $phone_number = $settings->value;

            // Fax Number
            $settings = Setting::where('key', '=', 'fax-number')->first();
            $fax_number = $settings->value;

            // Address
            $settings = Setting::where('key', '=', 'address')->first();
            $address = $settings->value;
			
			// Social - Facebook
            $settings = Setting::where('key', '=', 'social-facebook')->first();
            $social_facebook = $settings->value;
			
			// Social - Twitter
            $settings = Setting::where('key', '=', 'social-twitter')->first();
            $social_twitter = $settings->value;
			
			// Social - LinkedIn
            $settings = Setting::where('key', '=', 'social-linkedin')->first();
            $social_linkedin = $settings->value;
			
			// Social - GooglePlus
            $settings = Setting::where('key', '=', 'social-googleplus')->first();
            $social_googleplus = $settings->value;
			
			// Social - Instagram
            $settings = Setting::where('key', '=', 'social-instagram')->first();
            $social_instagram = $settings->value;
			
			// Social - Pinterest
            $settings = Setting::where('key', '=', 'social-pinterest')->first();
            $social_pinterest = $settings->value;
			
			// Social - Youtube
            $settings = Setting::where('key', '=', 'social-youtube')->first();
            $social_youtube = $settings->value;

			
            $view->with(array(
                'modules' => $modules,
				'company_name' => $company_name,
				'phone_number' => $phone_number,
                'email' => $email,
				'fax-number' => $fax_number,
				'address' => $address,
				'social_facebook' => $social_facebook,
				'social_twitter' => $social_twitter,
				'social_linkedin' => $social_linkedin,
				'social_googleplus' => $social_googleplus,
				'social_instagram' => $social_instagram,
				'social_pinterest' => $social_pinterest,
				'social_youtube' => $social_youtube,
            ));
        });
		
		//contact form validation
        Validator::extend(
            'recaptcha',
            'App\\Validators\\ReCaptcha@validate'
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
