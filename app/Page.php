<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Rutorika\Sortable\SortableTrait;

class Page extends Model
{
    use SortableTrait, Sortable;

    protected $table = 'pages';

    public $sortable = ['title', 'category_id', 'status'];

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function category()
    {
        return $this->belongsTo(PageCategory::class, 'category_id');
    }

    public function categorysort()
    {
        return $this->hasOne(PageCategory::class,'id','category_id');
    }

    public function parentPage()
    {
        return $this->belongsTo(Page::class, 'parent_page_id');
    }

    public function parentpagesort()
    {
        return $this->hasOne(Page::class, 'id');
    }

    public function childPages()
    {
        return $this->hasMany(Page::class, 'parent_page_id');
    }

    public function scopeFilter($query)
    {

        $filter = session()->get('pages-filter');
        $select = "";

        if($filter['category'] && $filter['category']!="all"){
            $select =  $query->where('category_id', $filter['category']);
        }

        if($filter['search']){
            $select =  $query->where('title','like', '%'.$filter['search'].'%');
        }

        return $select;
    }

    public function getUrlAttribute()
    {
        $special_url = SpecialUrl::where('item_id','=', $this->attributes['id'])->where('module','=','pages')->where('type','=','item')->first();
        if($special_url){
            return $special_url->url;
        }


        return 'pages/'.$this->category->slug.'/'.$this->attributes['slug'];
    }
}
