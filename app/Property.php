<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Rutorika\Sortable\SortableTrait;

class Property extends Model
{
    use SortableTrait, Sortable;

    protected $table = 'properties';

    public $sortable = ['title', 'category_id', 'status'];

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function category()
    {
        return $this->belongsTo(PropertyCategory::class, 'category_id');
    }

    public function categorysort()
    {
        return $this->hasOne(PropertyCategory::class,'id','category_id');
    }

    public function images()
    {
        return $this->hasMany(PropertyImage::class, 'property_id')->orderBy('position', 'asc');
    }

    public function floorplans()
    {
        return $this->hasMany(PropertyFloorplan::class, 'property_id')->orderBy('position', 'asc');
    }

    public function scopeFilter($query)
    {

        $filter = session()->get('properties-filter');
        $select = "";

        if($filter['category'] && $filter['category']!="all"){
            $select =  $query->where('category_id', $filter['category']);
        }

        if($filter['search']){
            $select =  $query->where('title','like', '%'.$filter['search'].'%');
        }

        return $select;
    }

    public function getUrlAttribute()
    {
        $special_url = SpecialUrl::where('item_id','=', $this->attributes['id'])->where('module','=','properties')->where('type','=','item')->first();
        if($special_url){
            return $special_url->url;
        }

        return 'properties/'.$this->category->slug.'/'.$this->attributes['slug'];
    }

    public function getOutdoorFeaturesArrayAttribute()
    {
        return $this->jsonToArray($this->attributes['outdoor_features']);
    }

    public function getIndoorFeaturesArrayAttribute()
    {
        return $this->jsonToArray($this->attributes['indoor_features']);
    }

    public function getHeatingCoolingArrayAttribute()
    {
        return $this->jsonToArray($this->attributes['heating_cooling']);
    }

    public function getEcoFriendlyFeaturesArrayAttribute()
    {
        return $this->jsonToArray($this->attributes['eco_friendly_features']);
    }

    private function jsonToArray($value)
    {
        $items = json_decode($value);
        $array = array();

        $count=0;
        foreach ($items as $item){
            array_push($array,$item->{$count});
            $count++;
        }

        return $array;
    }
}
