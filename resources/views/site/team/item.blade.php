@extends('site/layouts/app')

@section('content')

<div class="blog-masthead ">
    <div class="container">

      <div class="row no-gutters">
        @include('site/partials/sidebar-navigation')

        <div class="col-lg-9 blog-main">

          <div class="blog-post">                                                                                                          
			<h1>{{ $team_member->name }}</h1>

		    <div class="fluid-container">
               <div class="row no-gutters">
               
                    <div class="col-lg-8 col-md-8 col-sm-12 no-gutters">
						@if($team_member->job_title)
						<h2>{{ $team_member->job_title }}</h2>
						@endif
						@if($team_member->role)
						<h3>{{ $team_member->role }}</h3>
						@endif
						@if($team_member->phone)
						<strong>Phone</strong> : {{ $team_member->phone }}
						@endif
						@if($team_member->mobile)
						<strong>Mobile</strong> : {{ $team_member->mobile }}
						@endif
						@if($team_member->email)
						<strong>Email</strong> : {{ $team_member->email }}
						@endif
						@if($team_member->body)
						{!! $team_member->body !!}
						@endif
                    </div>
                                        
					@if ($team_member->photo)
						<div class='col-lg-4 col-md-4 col-sm-12'>  
						    <div class="team-img"><img src="{{ url('') }}{{$team_member->photo}}" alt="{{$team_member->name}}"></div>                                      
						</div>
					@endif
		    
		            <div class='col-12'>
                       <div class='btn-back'>
                          <a class='btn-back' href='{{ url('') }}/team/{{ $team_member->category->slug }}'><i  class='fa fa-chevron-left'></i> back</a>
					   </div>
                    </div>
                    
			    </div>
			 </div>								             
									
          </div><!-- /.blog-post -->
        </div><!-- /.blog-main -->

      </div><!-- /.row -->

    </div><!-- /.container -->
    </div><!-- /.blog-masthead -->
@endsection								