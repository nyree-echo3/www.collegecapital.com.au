@extends('site/layouts/app')

@section('content')

    <div class="blog-masthead ">
        <div class="container">

            <div class="row no-gutters">
                @include('site/partials/sidebar-navigation')

                <div class="col-lg-9 blog-main">

                    <div class="blog-post">
                        <h1>Our People</h1>
                        @if($items)
                            <div class="container">
                                <div class="row no-gutters">
                                    @php $currentCategory = "" @endphp

                                    @foreach($items as $item)
                                        @if ($currentCategory == "" || $currentCategory != $item->category->name)
                                            <div class="col-12">
                                                <h2 class="blog-post-title team-h2 {{ ($currentCategory != "" ? 'team-h2-gap' : '') }}">{{ $item->category->name }}</h2>
                                            </div>

                                            @php $currentCategory = $item->category->name @endphp
                                        @endif

                                        <div class="col-lg-4 col-md-6 team-item">
                                            @if ($item->role != "") <a href='{{ $item->role }}' target="_blank"> @endif

                                                <div class="team-a">
                                                    <div class="div-img">
                                                        <img src="{{ url('') }}/{{ $item->photo }}" alt="{{ $item->name }}">
                                                    </div>
                                                    <div class="team-txt">
                                                        <div class="team-name-band-name">{{ $item->name }}</div>
                                                        <div class="team-name-band-position">{{ $item->job_title }}</div>
                                                    </div>
                                                </div>
                                                @if ($item->role != "") </a> @endif
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @else
                            <p>Currently there is no team member to display.</p>
                        @endif

                    </div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div><!-- /.blog-masthead -->
@endsection
