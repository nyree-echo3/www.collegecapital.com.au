@extends('site/layouts/app')


@section('styles')
<style>
	main{
		padding-bottom:0px !important;
	}
</style>
@endsection

@section('content')

@include('site/partials/carousel')

<div class="container marketing">

<!-- Intro Text -->
<div class="row featurette no-gutters">
  <div class="col home-intro">
    <?php echo $home_intro_text; ?>
  </div>      
</div>

<div class="home-intro-icons">
   <div class="container">
	  <div class="row no-gutters">
	    <img src="{{ url('') }}/images/site/icons.png" title="Icons" alt="Icons">
		<!--
		<div class="col-lg-4 col-md-6 col-6">
			<img src="{{ url('') }}/images/site/icon1.png" title="Icon - Lenders 40+" alt="Icon - Lenders 40+">
		</div>
		
		<div class="col-lg-4 col-md-6 col-6">
			<img src="{{ url('') }}/images/site/icon2.png" title="Icon - Financial Products 100+" alt="Icon - Financial Products 100+">
		</div>
		
		<div class="col-lg-4 col-md-6 col-6">
			<img src="{{ url('') }}/images/site/icon5.png" title="Icon - Shareholder Broker Firms" alt="Icon - Shareholder Broker Firms">
		</div>
		
		<div class="col-lg-4 col-md-6 col-6">
			<img src="{{ url('') }}/images/site/icon3.png" title="Icon - Financial Products 100+" alt="Icon - Financial Products 100+">
		</div>
		
		<div class="col-lg-4 col-md-6 col-6">
			<img src="{{ url('') }}/images/site/icon5.png" title="Icon - Shareholder Broker Firms" alt="Icon - Shareholder Broker Firms">
		</div>
		
		<div class="col-lg-4 col-md-6 col-6">
			<img src="{{ url('') }}/images/site/icon6.png" title="Icon - Lenders 40+" alt="Icon - Lenders 40+">
		</div>
		-->
		
	  </div>	  
   </div>
</div>
  
</div>

@include('site/partials/index-team')
@include('site/partials/index-testimonials')
@include('site/partials/index-links')
@include('site/partials/index-partners')

@endsection
