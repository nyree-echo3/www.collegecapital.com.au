@extends('site/layouts/app')

@section('content')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row no-gutters">      
        @include('site/partials/sidebar-contact')
                     
        <div class="col-lg-9 blog-main">

          <div class="blog-post">   
               <h1>Contact</h1>    
               <p>Thank you for your enquiry. We will be in touch with you shortly.</p>             
            </div>
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
<div><!-- /.blog-masthead -->    
@endsection            
