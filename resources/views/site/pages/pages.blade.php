<?php
   // Set Meta Tags
   $meta_title_inner = $page->meta_title;
   $meta_keywords_inner = $page->meta_keywords;
   $meta_description_inner = $page->meta_description;
?>

@extends('site/layouts/app')

@section('content')

<div class="blog-masthead ">
    <div class="container">

      <div class="row no-gutters">
        @include('site/partials/sidebar-navigation')

        <div class="col-lg-9 blog-main">

          <div class="blog-post">
            @if (isset($page))
            <h1 class="blog-post-title">{{$page->title}}</h1>
            {!! $page->body !!}
            @endif
          </div><!-- /.blog-post -->
        </div><!-- /.blog-main -->

      </div><!-- /.row -->

    </div><!-- /.container -->
    </div><!-- /.blog-masthead -->
@endsection
