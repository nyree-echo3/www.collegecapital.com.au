@if (isset($home_team))
    <div class="home-team">
        <div class="panelNav">
            <h2>Our <strong>People</strong></h2>
            <h3>Our staff are the back-bone to each successful service. Our people come from a variety of backgrounds and professional experience are not only qualified and experienced, but are also passionate about what they do. Meet the team who make it all happen at College Capital.</h3>

            <div class="container-fluid">
                <div id="carouselTeam" class="carousel slide" data-ride="carousel" data-interval="9000">
                    <div id="carouselTeam-inner" class="carousel-inner row w-100 mx-auto" role="listbox">
                        @php
                            $counter = 0;
                        @endphp

                        @foreach ($home_team as $item)
                            <div class="carouselTeam-item carousel-item col-md-4 {{ ($counter == 0 ? 'active' : '') }} carousel-team-item">
                                @if ($item->role != "") <a href='{{ $item->role }}' target="_blank"> @endif
                                <div class="home-team-img">
                                    <img src="{{ url('') }}/{{$item->photo}}" class="img-fluid mx-auto d-block" alt="{{$item->name}}">
                                </div>

                                <div class="home-team-name">{{$item->name}}</div>
                                <div class="home-team-title">{{$item->job_title}}</div>
                                @if ($item->role != "") </a> @endif
                            </div>

                            @php
                                $counter++;
                            @endphp
                        @endforeach

                    </div>
                    <a class="carousel-control-prev" href="#carouselTeam" role="button" data-slide="prev">
                        <i class="fa fa-chevron-left fa-lg"></i> <span class="sr-only">Previous</span> </a>
                    <a class="carousel-control-next text-faded" href="#carouselTeam" role="button" data-slide="next">
                        <i class="fa fa-chevron-right fa-lg"></i> <span class="sr-only">Next</span> </a>
                </div>
            </div>

            <!-- For Mobile Only -->
            <div id="carouselTeam-resp" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">

                    <?php $counter = 0; ?>
                    @foreach ($home_team as $item)
                        <?php $counter++;  ?>

                        <div class="carousel-item {{ $counter == 1 ? ' active' : '' }}">
                            <img class="slide" src="{{ url('') }}/{{ $item->photo }}" alt="{{ $item->name }}">

                            <div class="home-team-name">{{$item->name}}</div>
                            <div class="home-team-title">{{$item->job_title}}</div>
                        </div>
                    @endforeach

                </div>

                <a class="carousel-control-prev" href="#carouselTeam-resp" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span> </a>
                <a class="carousel-control-next" href="#carouselTeam-resp" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span> </a>
            </div>
            <!-- END - For Mobile Only -->

        </div>
    </div>
@endif

@section('team-scripts')
    <script type="text/javascript">
        $("#carouselTeam").on("slide.bs.carousel", function (e) {
            var $e = $(e.relatedTarget);
            var idx = $e.index();
            var itemsPerSlide = 3;
            var totalItems = $(".carouselTeam-item").length;

            if (idx >= totalItems - (itemsPerSlide - 1)) {
                var it = itemsPerSlide - (totalItems - idx);
                for (var i = 0; i < it; i++) {
                    // append slides to end
                    if (e.direction == "left") {
                        $(".carouselTeam-item")
                            .eq(i)
                            .appendTo("#carouselTeam-inner");
                    } else {
                        $(".carouselTeam-item")
                            .eq(0)
                            .appendTo("#carouselTeam-inner");
                    }
                }
            }
        });
    </script>
@endsection