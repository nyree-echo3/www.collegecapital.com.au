<div class="col-lg-3 blog-sidebar">         
  <div class="sidebar-module">	
	<ol class="navsidebar list-unstyled">             
	  @foreach ($navigation as $item)		 
		 <li class='{{ ($_SERVER['REQUEST_URI'] == "/" . $item["url"] ? "active" : "") }} {{ (isset($page_type) && $item["slug"] == strtolower($page_type) ? "active" : "") }}'><a class="navsidebar" href="{{ url('').'/'.$item["url"] }}">{{ $item["display_name"] }}</a></li>

		 <ol class="navsidebar navsidebar-sub list-unstyled">  	
		    @if (isset($item["nav_sub"]) )	
				@foreach ($item["nav_sub"] as $item_sub)     

				 <li class='{{ ($_SERVER['REQUEST_URI'] == "/" . $item_sub["url"] ? "active" : "") }}'><a class="navsidebar" href="{{ url('').'/'.$item_sub["url"] }}">{{ $item_sub["title"] }}</a></li>
				@endforeach
			@endif						
		 </ol>
	  @endforeach 
	  
	  <li><a class="" href="https://ccap.collegecapital.com.au/users/login" target="_blank">Broker Login <img class="nav-link-login-side" src="{{ url('') }}/images/site/login-black.png" title="Broker Login" alt="Broker Login"></a></li>             
	</ol>
  </div>          
</div>