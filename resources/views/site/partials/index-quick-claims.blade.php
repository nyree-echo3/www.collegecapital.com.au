@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/formvalidation/dist/css/formValidation.css') }}">
@endsection

<div class="panelNav">
	<div class="home-quick-claims">
       <div class="row">
           <div class="col-lg-6 home-quick-claims-img">
              <img src="{{ url('') }}/images/site/pic-staff.jpg" title="Quick Quote" alt="Quick Quote">
		   </div>
		   
		   <div class="col-lg-6">
			   <form id="contact-form" method="post" action="{{ url('claims/save-message-quick') }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div id="claims-form-fields"></div>

					<div class="form-row">
						<div class="col-12 col-sm-10 g-recaptcha-container">
							<div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
							@if ($errors->has('g-recaptcha-response'))
								<div class="fv-help-block">{{ $errors->first('g-recaptcha-response') }}</div>
							@endif
						</div>
					</div>
					<button type="submit" class="btn-submit">Submit Enquiry</button>
				</form>  
		    </div>
		</div>
	</div>
</div>

@section('scripts')
    <script src="{{ asset('/components/formBuilder/dist/form-render.min.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/formValidation.js') }}"></script>
    <script src="{{ asset('/components/formvalidation/dist/js/plugins/Bootstrap.js') }}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#claims-form-fields').formRender({
                dataType: 'json',
                formData: {!! $home_quick_claims !!},
                notify: {
                    success: function(message) {

                        FormValidation.formValidation(
                            document.getElementById('quick-claims-form'),
                            {
                                plugins: {
                                    declarative: new FormValidation.plugins.Declarative({
                                        html5Input: true,
                                    }),
                                    submitButton: new FormValidation.plugins.SubmitButton(),
                                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                                    bootstrap: new FormValidation.plugins.Bootstrap(),
                                    icon: new FormValidation.plugins.Icon({
                                        valid: 'fa fa-check',
                                        invalid: 'fa fa-times',
                                        validating: 'fa fa-refresh',
                                    })
                                },
                            }
                        );

                    }
                }
            });

        });
    </script>
@endsection
