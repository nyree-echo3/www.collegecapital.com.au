<div id="myCarousel" class="carousel slide" data-ride="carousel">
<!--<ol class="carousel-indicators">
    <?php $counter = 0; ?>
@foreach($images as $image)
    <li data-target="#myCarousel" data-slide-to="{{ $counter }}" class="{{ $counter == 0 ? 'active' : '' }}"></li>
       <?php $counter++;  ?>
@endforeach
        </ol>-->

    <div class="carousel-inner">

        <?php $counter = 0; ?>
        @foreach($images as $image)
            <?php $counter++;  ?>

            <div class="carousel-item {{ $counter == 1 ? ' active' : '' }}">
                @if (strtolower(substr($image->location, -3)) == "mp4")
                    <div class="video-container">
                        <video autoplay loop muted id="slider-video">
                            <source src="{{ url('') }}/{{ $image->location }}" type="video/mp4"/>
                        </video>
                    </div>
                @else
                    <img class="slide-img slide" src="{{ url('') }}/{{ $image->location }}" alt="{{ $image->title }}">
                    <img class="slide-img-resp slide" src="{{ url('') }}/{{ substr($image->location, 0, -4) }}-mobile{{ substr($image->location, -4) }}" alt="{{ $image->title }}">
                @endif

                <div class="container">
                    <div class="carousel-caption">
                        @if ( $image->title != "") <h2>{{ $image->title }}</h2> @endif
                        @if ( $image->description != "") <p>{{ $image->description }}</p> @endif
                        @if ( $image->url != "")<p>
                            <a class="btn btn-lg btn-primary" href="{{ $image->url }}" role="button">Click for more</a></p>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach

    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

@section('slider-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            document.querySelector('#slider-video').playbackRate = 0.5;
        });
    </script>
@endsection