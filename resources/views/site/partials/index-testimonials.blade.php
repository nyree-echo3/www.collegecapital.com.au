@if (isset($home_testimonials))
	<div class="home-testimonials"> 
	    <div class="panelNav">
			<h2>What our <strong>clients</strong> say</h2>			
			<div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="10000">				  
				  <div class="carousel-inner">

						@php
					       $counter = 0;
				        @endphp
				  
						@foreach($home_testimonials as $home_testimonial) 
							@php
						       $counter++;
					        @endphp

							<div class="carousel-item {{ $counter == 1 ? ' active' : '' }}">
							   <div class="testimonial-desc">
								  {!! $home_testimonial->description !!}															  
							   </div>	
							   
							   <div class="testimonial-person">
								  - {{ $home_testimonial->person }}															  
							   </div>							 							  	  						 							  	  
							</div>
						@endforeach

				  </div>
				</div>
		</div>
	</div>	 
@endif