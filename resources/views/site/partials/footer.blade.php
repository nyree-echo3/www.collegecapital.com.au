<footer class='footer'>
    <div class='footerContainer'>
        <div class="panelNav">
            <div class="row">
                <div class="col-lg-2">
                    <div id="footer-logo">
                        <a href="{{ url('') }}" title="{{ $company_name }}"><img src="{{ url('') }}/images/site/logo-bottom.png" title="{{ $company_name }}" alt="{{ $company_name }}"></a>
                    </div>
                </div>

                <div class="col-lg-10">
                    <div id="footer-caption">
                        <span class="footer-caption-1">Trusted Finance</span>
                        <span class="footer-caption-2">Professionals</span>
                    </div>

                    <div class="footer-icons">
                        <div id="footer-logos">
                            <a href="https://www.cafba.com.au/" target="_blank"><img src="{{ url('') }}/images/site/logo-cafba.png" title="CAFBA" alt="CAFBA"></a>
                            <a href="https://www.fbaa.com.au/" target="_blank"><img src="{{ url('') }}/images/site/logo-fbaa.png" title="FBAA" alt="FBAA"></a>
                            <a href="https://www.afca.org.au/" target="_blank"><img src="{{ url('') }}/images/site/logo-afca.png" title="AFCA" alt="AFCA"></a>
                        </div>

                        <div id="footer-social">
                            <div class="footer-social-txt">Follow us on</div>

                            <div class="footer-social-icon">
                                @if ( $social_facebook != "")<a href="{{ $social_facebook }}" target="_blank"><i class='fab fa-facebook-f'></i></a> @endif
                                @if ( $social_twitter != "")<a href="{{ $social_twitter }}" target="_blank"><i class='fab fa-twitter'></i></a> @endif
                                @if ( $social_linkedin != "")<a href="{{ $social_linkedin }}" target="_blank"><i class='fab fa-linkedin-in'></i></a> @endif
                                @if ( $social_googleplus != "")<a href="{{ $social_googleplus }}" target="_blank"><i class='fab fa-google-plus-g'></i></a> @endif
                                @if ( $social_instagram != "")<a href="{{ $social_instagram }}" target="_blank"><i class='fab fa-instagram'></i></a> @endif
                                @if ( $social_pinterest != "")<a href="{{ $social_pinterest }}" target="_blank"><i class='fab fa-pinterest-p'></i></a> @endif
                                @if ( $social_youtube != "")<a href="{{ $social_youtube }}" target="_blank"><i class='fab fa-youtube-square'></i></a> @endif
                                <!--<a href="{{ url('') }}"/rss.xml" target="_blank"><i class='fa fa-rss'></i></a>
					            <a href="{{ url('') }}"/contact" ><i class='fa fa-envelope'></i></a>-->
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div id="footer-txt">
                @if ( $company_name != "")
                    <a href="{{ url('') }}">&copy; {{ date('Y') }} {{ $company_name }}</a> | @endif
                <a href="{{ url('') }}/pages/other/privacy-statement">Privacy Statement</a> |
                <a href="{{ url('') }}/pages/other/whistleblower-policy">Whistleblower Policy</a> |
                <a href="https://www.echo3.com.au" target="_blank">Website by Echo3</a>
            </div>
        </div>
    </div>
</footer>