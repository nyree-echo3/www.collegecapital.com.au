<!-- Slideshow Cycle http://jquery.malsup.com/cycle2/demo/-->
<script src="{{ url('') }}/js/site/jquery-1.22.4.min.js"></script>
<script type="text/javascript" src="{{ url('') }}/js/site/jquery.cycle2.js"></script>
<script src="{{ url('') }}/js/site/jquery.cycle2.carousel.min.js"></script>

<script>
$.fn.cycle.defaults.autoSelector = '.links-carousel';
</script>

<div class="home-links-panel">
   <div class="panelNav">
	  <h2>Our <strong>Partners</strong></h2>
	  <p>Our lender panel for better business outcomes</p>	    		   
		

 	  <div class="links-carousel" 
	     data-cycle-fx=carousel
	     data-cycle-carousel-visible=4
		data-cycle-timeout=1
		data-cycle-speed=5000
		data-cycle-throttle-speed=true
		data-cycle-easing=linear
		data-cycle-slides=span
	  >

			@foreach ($home_links as $home_link)
			   <span><a href='http://{{ $home_link->url }}' target='_blank' title='{{ $home_link->name }}'><img src='{{ url('') }}/{{ $home_link->image }}' ></a></span>		
			@endforeach

	 </div>
   </div> 
</div>
	