<div class="col-sm-3 offset-sm-1 blog-sidebar">         
  <div class="sidebar-module">
	<h4>Documents</h4>
	<ol class="navsidebar list-unstyled">   
      @if (isset($side_nav))           
		  @foreach ($side_nav as $item)	 
			 <li class='{{ ( sizeof($items) > 0 && $items[0]->category_id == $item->id  ? "active" : "") }}'><a class="navsidebar" href="{{ url('').'/'.$item->url }}">{{ $item->name }}</a></li>
		  @endforeach            
	  @endif  
	</ol>
  </div>          
</div>