<div class="container-fluid home-partners">
	<div class="row justify-content-center">
		<div class="home-partner-box col-sm-12 col-md-4">
			<p>OUR <strong>TECHNOLOGY</strong> PARTNER</p>
			<a href="https://www.kubio.com.au/" target="_blank">
				<img src="{{ url('images/site/logo-kubio.png') }}" />
			</a>
		</div>
		<div class="home-partner-box col-sm-12 col-md-4 offset-md-1">
			<p>OUR <strong>COMPLIANCE</strong> PARTNER</p>
			<a href="https://www.safetrac.com.au/" target="_blank">
				<img src="{{ url('images/site/logo-safetrac.png') }}" />
			</a>
		</div>
	</div>
</div>