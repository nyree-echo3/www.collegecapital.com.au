<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-light btco-hover-menu navbar-custom">

    <div class="broker-login-icon">
        <a href="https://ccap.collegecapital.com.au/users/login" target="_blank">
            <img src="{{ url('/images/site/login-blue-index.png') }}" title="Broker Login" alt="Broker Login">
        </a>
    </div>

    <div class="navbar-logo">
        <a href="{{ url('') }}" title="{{ $company_name }}"><img src="{{ url('') }}/images/site/logo.png" title="{{ $company_name }}" alt="{{ $company_name }}"></a>
    </div>

    <button class="navbar-toggler custom-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
        <!--<li class="nav-item {{ (!isset($page_type) ? "active" : "") }}">
				<a class="nav-link" href="{{ url('') }}"><i class='fa fa-home'></i> <span class="sr-only">(current)</span></a>
			</li>-->
            @if(count($navigation))
                @foreach($navigation as $nav_item)
                    <li class="nav-item dropdown">
                        <a class="nav-link {{ (isset($page_type) && (($page_type == "Pages" && $category[0]->slug == $nav_item["slug"]) || ($page_type != "Pages" && $page_type == $nav_item["name"])) ? "active" : "") }}" href="{{ url('').'/'.$nav_item["url"] }}" id="navbarDropdownMenuLink">
                            {{ $nav_item["display_name"] }}
                        </a>

                        @if(isset($nav_item["nav_sub"]) && sizeof($nav_item["nav_sub"]) > 1)
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">

                                @foreach($nav_item["nav_sub"] as $nav_sub)

                                    <li>
                                        <a class="dropdown-item" href="{{ url('').'/'.$nav_sub["url"] }}">{{ (isset($nav_sub["title"]) ? $nav_sub["title"] : $nav_sub["name"]) }}</a>

                                        @if(isset($nav_item["nav_sub_sub"]))
                                            <ul class="dropdown-menu">
                                                @foreach($nav_item["nav_sub_sub"] as $nav_sub_sub)
                                                    @if($nav_sub_sub["parent_page_id"] == $nav_sub["id"])
                                                        <li>
                                                            <a class="dropdown-item" href="{{ url('').'/'.$nav_sub_sub["url"] }}">{{ (isset($nav_sub_sub["title"]) ? $nav_sub_sub["title"] : $nav_sub_sub["name"]) }}</a>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        @endif

                                    </li>
                                @endforeach

                            </ul>
                        @endif

                    </li>
            @endforeach
        @endif

        <!--<li class="nav-item">
			   <a class="nav-link" href="https://ccap.collegecapital.com.au/users/login" target="_blank">
			      <img class="nav-link-login" src="{{ url('') }}/images/site/login.png" title="Broker Login" alt="Broker Login">
			      <img class="nav-link-login-resp" src="{{ url('') }}/images/site/login-blue.png" title="Broker Login" alt="Broker Login">
			      Broker Login
			   </a>
		    </li>                       			                        		-->
        </ul>
    </div>

    <div class='navbar-contacts'>
    <!--
	  <div><span class="home-insurance-you"><strong>Call to speak to licensed agent now</strong></span></div>
	  <a href='tel:{{ str_replace(' ', '', $phone_number) }}'>{{ $phone_number }}</a>			
	  -->
    </div>

<!--<div class='navbar-contacts-resp'>
        <a href='tel:{{ str_replace(' ', '', $phone_number) }}'><i class="fas fa-phone-alt"></i></a>
    </div>-->
</nav>