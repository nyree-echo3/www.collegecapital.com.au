<!DOCTYPE html>
<html lang="en"> 
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Echo3</title>
    <link href="{{ asset('/components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/components/font-awesome/web-fonts-with-css/css/fontawesome-all.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">   
    <link rel="stylesheet" href="{{ asset('css/site/holding.css') }}">
  </head>

  <body>
  <div class="vertical-center" >
      <div class="container">

        <div class="row row-eq-height">
          <div class="col-md-7">
            <p class="heading">Hello,</p>
            <p class="text">Thanks for visiting {{ url('') }}.</p>
            <p class="text">We at Echo3 are currently hard at work hatching a new website which will be up soon.</p>
            <p class="text">Or sign up here and {{ $company_name }} will notify you when they are ready to launch.</p>

            <form id="contact-form" method="post" action="{{ url('contact/save-message-golive') }}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="input-group col-xs-7">
                <input name="email" id="email" type="text" class="form-control" placeholder="your email here">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="submit">NOTIFY ME</button>
                </span>
              </div>
            </form>

            <p class="text">In the meantime, if you have some plans to hatch your own new venture – <a href="https://www.echo3.com.au/">pop over to our site</a> to see what else we’ve been incubating.</p>
            <div>
              <span class="text">Follow us on</span> 
              <a href="https://www.facebook.com/echo3media" target="_blank" class="sm-icon"><i class="fab fa-facebook-square"></i></a>
              <a href="http://instagram.com/echo3media" target="_blank" class="sm-icon"><i class="fab fa-instagram"></i></a>
              <a href="http://www.linkedin.com/company/echo3" target="_blank" class="sm-icon"><i class="fab fa-linkedin"></i></a>
            </div>
            <div>
              <a href="https://www.echo3.com.au/" target="_blank"><img src="{{ url('') }}/images/site/echo3logo.png" class="logo" alt="Echo3"></a>
            </div>
          </div>
          <div class="col-md-5 hidden-xs hidden-sm">
            <img src="{{ url('') }}/images/site/egg.gif" class="egg center-block" alt="Echo3egg">
          </div>
        </div>
      </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="fontawesome/fontawesome-all.min.js"></script>
  </body>
</html>