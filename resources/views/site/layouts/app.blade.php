@php
   if(isset($mode) && $mode == 'preview')
     $meta_title_inner = "Preview Page";
@endphp
    
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="keywords" content="{{ (isset($meta_keywords_inner) ? $meta_keywords_inner : $meta_keywords) }}">
    <meta name="description" content="{{ (isset($meta_description_inner) ? $meta_description_inner : $meta_description) }}">
	<meta name="author" content="Echo3 Media">
	<meta name="web_author" content="www.echo3.com.au">
	<meta name="date" content="{{ $live_date }}" scheme="DD-MM-YYYY">
	<meta name="robots" content="all" >
    <title>{{ (isset($meta_title_inner) ? $meta_title_inner : $meta_title) }}</title>

    <link href="https://fonts.googleapis.com/css?family=Arimo:400,700&display=swap" rel="stylesheet">
    <link href="{{ asset('/components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/components/font-awesome/css/all.css') }}">
    <link href="{{ asset('/css/site/carousel.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/site/bootstrap-4-navbar.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/jquery-ui/themes/base/all.css') }}">
    <link rel="stylesheet" href="{{ asset('css/site/general.css?v=0.1') }}">

    <link rel="shortcut icon" href="{{ url('') }}/favicon.ico?" >
    <link rel="apple-touch-icon" href="{{ url('') }}/apple-icon.png" >
   
    <!-- Google Analytics --> 
    {!! $google_analytics !!} 
    
    @yield('styles')
            
  </head>
  <body>        
   
    <header>
    @include('site/partials/preview')
    @include('site/partials/navigation')
    </header>

    <main role="main">       
        @yield('content')
        @include('site/partials/footer')
    </main>

<script src="{{ asset('/components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('/components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/components/jquery-ui/jquery-ui.js') }}"></script>
<script src="{{ asset('js/site/bootstrap-4-navbar.js') }}"></script>

@yield('scripts')
@yield('inline-scripts')
@yield('team-scripts')
@yield('slider-scripts')
<script type="text/javascript">
  $( document ).ready(function() {
    $('.juiAccordion').accordion({
        heightStyle: "content"
    });
  });
</script>
</body>
</html>
