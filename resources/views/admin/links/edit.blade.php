@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/links') }}"><i class="fas fa-link"></i> {{ $display_name }}</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/links/update') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" value="{{ $link->id }}">
                            <div class="box-body">
                                <div class="form-group{{ ($errors->has('name')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Name *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="name" name="name"
                                               placeholder="Name" value="{{ old('name',$link->name) }}">
                                        @if ($errors->has('name'))
                                            <small class="help-block">{{ $errors->first('name') }}</small>
                                        @endif
                                    </div>
                                </div>
                                                                     
                                @php
                                    if(old('category_id')!=''){
                                        $category_id = old('category_id');
                                    }else{
                                        $category_id = $link->category_id;
                                    }
                                @endphp
                                <div class="form-group{{ ($errors->has('category_id')) ? ' has-error' : '' }}"
                                     id="category_selector">
                                    <label class="col-sm-2 control-label">Category *</label>

                                    <div class="col-sm-10">
                                        <select name="category_id" class="form-control select2"
                                                data-placeholder="All" style="width: 100%;">
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}"{{ ($category_id == $category->id) ? ' selected="selected"' : '' }}>{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                                                      
                                <div class="form-group {{ ($errors->has('description')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Description</label>

                                    <div class="col-sm-10">
                                        <textarea id="description" name="description" rows="10" cols="80"
                                                  style="height: 500px;">{{ old('description', $link->description) }}</textarea>
                                        @if ($errors->has('description'))
                                            <small class="help-block">{{ $errors->first('description') }}</small>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ ($errors->has('url')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">URL *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="url" name="url"
                                               placeholder="URL" value="{{ old('url',$link->url) }}">
                                        @if ($errors->has('url'))
                                            <small class="help-block">{{ $errors->first('url') }}</small>
                                        @endif
                                    </div>
                                </div>
                               
                                @php
                                    if(count($errors)>0){
                                       if(old('image')!=''){
                                        $image_image = old('image');
                                       }else{
                                        $image_image = '';
                                       }
                                    }else{
                                        $image_image = $link->image;
                                    }
                                @endphp
                                <div class="form-group {{ ($errors->has('image')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Image</label>
                                    <div class="col-sm-10">
                                        <input type="hidden" id="image" name="image"
                                               value="{{ old('image',$image_image) }}">
                                        <button id="image-popup" type="button" class="btn btn-info btn-sm">Upload
                                            Image
                                        </button>
                                        @php
                                            $class = ' invisible';
                                            if($image_image!=''){
                                                $class = '';
                                            }
                                        @endphp
                                        <button id="remove-image" type="button"
                                                class="btn btn-danger btn-sm{{ $class }}">Remove Image
                                        </button>
                                        <br/><br/>
                                        <span id="added_image">
                                        @if($image_image!='')
                                                <image src="{{ old('image',$image_image) }}"/>
                                            @endif
                                        </span>
                                        @if ($errors->has('image'))
                                            <small class="help-block">{{ $errors->first('image') }}</small>
                                        @endif
                                    </div>                                
                               </div>
                            
                            </div>
                            @php
                                if(count($errors)>0){
                                   if(old('live')=='on'){
                                    $status = 'active';
                                   }else{
                                    $status = '';
                                   }
                                }else{
                                    $status = $link->status;
                                }
                            @endphp
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status *</label>
                                <div class="col-sm-10">
                                    <label>
                                        <input class="page_status" type="checkbox" data-toggle="toggle" data-size="mini"
                                               name="live" {{ $status == 'active' ? ' checked' : null }}>
                                    </label>
                                </div>
                            </div>

                            <div class="box-footer">
                                <a href="{{ url('dreamcms/links') }}" class="btn btn-info pull-right"
                                   data-toggle=confirmation data-title="Your changes will be lost! Are you sure?"
                                   data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                   data-btn-cancel-label="No">Cancel</a>                                                                   
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save_close">
                                    Save & Close
                                </button>                                                                
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save">Save
                                </button>                                                                                                    								
                            </div>
                            
                            <div class="box-footer">                                
                                                                                           
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>

    <div class="modal fade" id="change-slug">
        <div class="modal-dialog">
            <div class="modal-content">               
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" id="save-seo" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            CKEDITOR.replace('description');
            $(".select2").select2();

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $('input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });

            $('input[type="radio"].minimal').on('ifChecked', function (event) {

                if ($(this).val() == 'parent') {
                    $('#category_selector').show();
                    $('#page_selector').hide();
                }

                if ($(this).val() == 'child') {
                    $('#category_selector').hide();
                    $('#page_selector').show();
                }

            });            
			
			$("#image-popup").click(function () {
                openPopup();
            });

            $("#remove-image").click(function () {
                $('#added_image').html('');
                $('#remove-image').addClass('invisible')
                $('#image').val('');
            });

        });
		
		function openPopup() {
            CKFinder.popup({
                chooseFiles: true,
                onInit: function (finder) {
                    finder.on('files:choose', function (evt) {
                        var file = evt.data.files.first();
                        $('#added_image').html('<image src="' + base_url + file.getUrl() + '">');
                        $('#remove-image').removeClass('invisible');
                        $('#image').val(file.getUrl());

                    });
                    finder.on('file:choose:resizedImage', function (evt) {
                        $('#added_image').html('<image src="' + base_url + evt.data.resizedUrl + '">');
                        $('#remove-image').removeClass('invisible');
                        $('#image').val(evt.data.resizedUrl);
                    });
                }
            });
        }
    </script>
@endsection