@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/links') }}"><i class="fas fa-link"></i> {{ $display_name }}</a></li>
                <li><a href="{{ url('dreamcms/links/categories') }}"> Categories</a></li>
            </ol>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Categories</h3>

                    @can('add-links-category')
                    <div class="pull-right box-tools">
                        <a href="{{ url('dreamcms/links/add-category') }}" type="button" class="btn btn-info btn-sm"
                           data-widget="add">Add New
                            <i class="fa fa-plus"></i>
                        </a>
                    </div>
                    @endcan
                </div>
                <div class="box-body">
                    @if(count($categories))
                        <table class="table table-hover">
                            <tr>
                                <th>Category</th>
                                <!--<th>Status</th>-->
                                <th class="hd-text-right">Actions</th>
                            </tr>
                            @foreach($categories as $category)
                                <tr>
                                    <td>{{ $category->name }}</td>
                                    <!--<td><input id="category_{{ $category->id }}" data-id="{{ $category->id }}"
                                               class="category_status" type="checkbox" data-toggle="toggle"
                                               data-size="mini"{{ $category->status == 'active' ? ' checked' : null }}>
                                    </td>-->
                                    <td>
                                        <div class="pull-right">
                                        @can('edit-links-category')
                                            <a href="{{ url('dreamcms/links/'.$category->id.'/edit-category') }}"
                                               class="tool" data-toggle="tooltip" title="Edit">
                                                <i class="fa fa-edit"></i></a>
                                        @endcan
                                        @can('delete-links-category')
                                            <a href="{{ url('dreamcms/links/'.$category->id.'/delete-category') }}"
                                               class="tool" data-toggle=confirmation data-title="Are you sure?"
                                               data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                               data-btn-cancel-label="No"><i class="far fa-trash-alt"></i></a>
                                        @endcan
                                        </div>
                                    </td>

                                </tr>
                            @endforeach
                        </table>
                    @else
                        No records
                    @endif
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $('.category_status').change(function () {
                $.ajax({
                    type: "POST",
                    url: $(this).data('id') + "/change-category-status",
                    data: {
                        'status': $(this).prop('checked')
                    },
                    success: function (response) {
                        if (response.status == "success") {
                            toastr.options = {"closeButton": true}
                            toastr.success('Status has been changed');
                        }
                    }
                });
            });
        });
    </script>
@endsection