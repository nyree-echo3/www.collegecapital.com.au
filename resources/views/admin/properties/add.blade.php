@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/jquery-ui/themes/ui-darkness/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/datepicker/datepicker3.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/timepicker/bootstrap-timepicker.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/properties') }}"><i class="fa fa-clipboard"></i> {{ $display_name }}</a></li>
                <li class="active">Add New</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add New</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/properties/store') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="box-body">

                                <div class="nav-tabs-custom">

                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#cms-details" data-toggle="tab">CMS Details</a></li>
                                        <li><a href="#listing-details" data-toggle="tab">Listing Details</a></li>
                                        <li><a href="#about-the-property" data-toggle="tab">About the Property</a></li>
                                        <li><a href="#listing-copy-and-images" data-toggle="tab">Listing Copy and Images</a></li>
                                        <li><a href="#create-inspection-times" data-toggle="tab">Create Inspection Times</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div id="cms-details" class="tab-pane active">

                                            <div class="form-group{{ ($errors->has('title')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Title *</label>

                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="title" id="title" placeholder="Title" value="{{ old('title') }}">
                                                    @if ($errors->has('title'))
                                                        <small class="help-block">{{ $errors->first('title') }}</small>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ ($errors->has('slug')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">SEO Name *</label>
                                                <div class="col-sm-10">

                                                    <div class="input-group">
                                                        <input type="text" id="slug" name="slug" class="form-control" value="{{ old('slug') }}" readonly>
                                                        <span class="input-group-btn">
                                                            <button type="button" class="btn btn-flat btn-info" data-toggle="modal" data-target="#change-slug">Change SEO Name</button>
                                                        </span>
                                                    </div>

                                                    @if ($errors->has('slug'))
                                                        <small class="help-block">{{ $errors->first('slug') }}</small>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ ($errors->has('meta_title')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Meta Title *</label>

                                                <div class="col-sm-10">
                                                    <div class="input-group">
                                                        <input type="text" id="meta_title" name="meta_title" class="form-control" placeholder="Meta Title" value="{{ old('meta_title') }}" maxlength="56">
                                                        <span class="input-group-btn">
                                                  <button type="button" class="btn btn-flat btn-info" data-toggle="modal"
                                                          id="copy-title" name="copy-title">Copy From Title
                                                  </button>
                                                </span>
                                                    </div>
                                                    <div id="c_count_meta_title">0 characters | 56 characters left</div>
                                                    @if ($errors->has('meta_title'))
                                                        <small class="help-block">{{ $errors->first('meta_title') }}</small>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ ($errors->has('meta_keywords')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Meta Keywords *</label>

                                                <div class="col-sm-10">
                                                    <textarea class="form-control" rows="3" name="meta_keywords" placeholder="Meta Keywords">{{ old('meta_keywords') }}</textarea>
                                                    @if ($errors->has('meta_keyword'))
                                                        <small class="help-block">{{ $errors->first('meta_keywords') }}</small>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ ($errors->has('meta_description')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Meta Description *</label>

                                                <div class="col-sm-10">
                                                    <textarea class="form-control" rows="3" id="meta_description" name="meta_description" maxlength="250" placeholder="Meta Description">{{ old('meta_description') }}</textarea>
                                                    <div id="c_count_meta_description">0 characters | 250 characters left |
                                                        0
                                                        words
                                                    </div>
                                                    @if ($errors->has('meta_description'))
                                                        <small class="help-block">{{ $errors->first('meta_description') }}</small>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ ($errors->has('special_url')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Special URL</label>

                                                <div class="col-sm-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">{{ env('APP_URL') }}/</span>
                                                        <input type="text" name="special_url" class="form-control"
                                                               value="{{ old('special_url') }}">
                                                    </div>
                                                    <div id="c_count_meta_description"></div>
                                                    @if ($errors->has('special_url'))
                                                        <small class="help-block">{{ $errors->first('special_url') }}</small>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button type="button" class="btn btn-default tab-next pull-right">Next <i class="fas fa-arrow-right"></i></button>
                                                </div>
                                            </div>

                                        </div>

                                        <div id="listing-details" class="tab-pane">

                                            <div class="form-group{{ ($errors->has('category_id')) ? ' has-error' : '' }}"
                                                 id="category_selector">
                                                <label class="col-sm-2 control-label">Category *</label>

                                                <div class="col-sm-10{{ ($errors->has('category_id')) ? ' has-error' : '' }}">
                                                    @if(count($categories)>0)
                                                        <select name="category_id" class="form-control select2"
                                                                data-placeholder="All" style="width: 100%;">
                                                            @foreach($categories as $category)
                                                                <option value="{{ $category->id }}"{{ (old('category_id') == $category->id) ? ' selected="selected"' : '' }}>{{ $category->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    @else
                                                        <div class="callout callout-danger">
                                                            <h4>No category found!</h4>
                                                            <a href="{{ url('dreamcms/properties/add-category') }}">Please click here to add category</a>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ ($errors->has('property_type')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Property Type *</label>
                                                <div class="col-sm-10{{ ($errors->has('property_type')) ? ' has-error' : '' }}">
                                                    <select name="property_type" class="form-control select2" data-placeholder="All" style="width: 100%;">
                                                        <option value="Acreage/Semi-Rural"{{ (old('property_type') == 'Acreage/Semi-Rural') ? ' selected="selected"' : '' }}>Acreage/Semi-Rural</option>
                                                        <option value="Alpine"{{ (old('property_type') == 'Alpine') ? ' selected="selected"' : '' }}>Alpine</option>
                                                        <option value="Apartment"{{ (old('property_type') == 'Apartment') ? ' selected="selected"' : '' }}>Apartment</option>
                                                        <option value="Block Of Units"{{ (old('property_type') == 'Block Of Units') ? ' selected="selected"' : '' }}>Block Of Units</option>
                                                        <option value="Duplex/Semi-detached"{{ (old('property_type') == 'Duplex/Semi-detached') ? ' selected="selected"' : '' }}>Duplex/Semi-detached</option>
                                                        <option value="Flat"{{ (old('property_type') == 'Flat') ? ' selected="selected"' : '' }}>Flat</option>
                                                        <option value="House"{{ (old('property_type') == 'House') ? ' selected="selected"' : '' }}>House</option>
                                                        <option value="Retirement Living"{{ (old('property_type') == 'Retirement Living') ? ' selected="selected"' : '' }}>Retirement Living</option>
                                                        <option value="Serviced Apartment"{{ (old('property_type') == 'Serviced Apartment') ? ' selected="selected"' : '' }}>Serviced Apartment</option>
                                                        <option value="Studio"{{ (old('property_type') == 'Studio') ? ' selected="selected"' : '' }}>Studio</option>
                                                        <option value="Terrace"{{ (old('property_type') == 'Terrace') ? ' selected="selected"' : '' }}>Terrace</option>
                                                        <option value="Townhouse"{{ (old('property_type') == 'Townhouse') ? ' selected="selected"' : '' }}>Townhouse</option>
                                                        <option value="Unit"{{ (old('property_type') == 'Unit') ? ' selected="selected"' : '' }}>Unit</option>
                                                        <option value="Villa"{{ (old('property_type') == 'Villa') ? ' selected="selected"' : '' }}>Villa</option>
                                                        <option value="Warehouse"{{ (old('property_type') == 'Warehouse') ? ' selected="selected"' : '' }}>Warehouse</option>
                                                        <option value="Other"{{ (old('property_type') == 'Other') ? ' selected="selected"' : '' }}>Other</option>
                                                    </select>
                                                    @if ($errors->has('property_type'))
                                                        <small class="help-block">{{ $errors->first('property_type') }}</small>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ ($errors->has('new_or_established')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">New or Established</label>
                                                <div class="col-sm-10{{ ($errors->has('new_or_established')) ? ' has-error' : '' }}">
                                                    <label class="radio-label">
                                                        <input type="radio" name="new_or_established" class="minimal" value="Established property" {{ (old('new_or_established') == 'Established property' || old('new_or_established') == "") ? ' checked="checked"' : '' }}>
                                                        Established property
                                                    </label>
                                                    <label class="radio-label">
                                                        <input type="radio" name="new_or_established" class="minimal" value="New construction" {{ old('new_or_established') == 'New construction' ? ' checked="checked"' : '' }}>
                                                        New construction
                                                    </label>
                                                    @if ($errors->has('new_or_established'))
                                                        <small class="help-block">{{ $errors->first('property_type') }}</small>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ ($errors->has('lead_agent_id')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Lead Agent</label>
                                                <div class="col-sm-10{{ ($errors->has('lead_agent_id')) ? ' has-error' : '' }}">
                                                    @if(count($agents)>0)
                                                        <select name="lead_agent_id" class="form-control select2" data-placeholder="All" style="width: 100%;">
                                                            <option value=""{{ (old('lead_agent_id') == 'select') ? ' selected="selected"' : '' }}>Please Select</option>
                                                            @foreach($agents as $agent)
                                                                <option value="{{ $agent->id }}"{{ (old('lead_agent_id') == $agent->id) ? ' selected="selected"' : '' }}>{{ $agent->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    @else
                                                        <div class="callout callout-danger">
                                                            <h4>No agent found!</h4>
                                                            <a href="{{ url('dreamcms/team/add') }}">Please click here to add agent</a>
                                                        </div>
                                                    @endif
                                                    @if ($errors->has('lead_agent_id'))
                                                        <small class="help-block">{{ $errors->first('property_type') }}</small>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ ($errors->has('dual_agent_id')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Dual Agent</label>
                                                <div class="col-sm-10{{ ($errors->has('dual_agent_id')) ? ' has-error' : '' }}">
                                                    @if(count($agents)>0)
                                                        <select name="dual_agent_id" class="form-control select2" data-placeholder="All" style="width: 100%;">
                                                            <option value=""{{ (old('dual_agent_id') == 'select') ? ' selected="selected"' : '' }}>Please Select</option>
                                                            @foreach($agents as $agent)
                                                                <option value="{{ $agent->id }}"{{ (old('dual_agent_id') == $agent->id) ? ' selected="selected"' : '' }}>{{ $agent->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    @else
                                                        <div class="callout callout-danger">
                                                            <h4>No agent found!</h4>
                                                            <a href="{{ url('dreamcms/team/add') }}">Please click here to add agent</a>
                                                        </div>
                                                    @endif
                                                    @if ($errors->has('dual_agent_id'))
                                                        <small class="help-block">{{ $errors->first('property_type') }}</small>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ ($errors->has('authority')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Authority</label>
                                                <div class="col-sm-10{{ ($errors->has('authority')) ? ' has-error' : '' }}">
                                                    <select name="authority" class="form-control select2" data-placeholder="All" style="width: 100%;">
                                                        <option value=""{{ (old('authority') == 'select') ? ' selected="selected"' : '' }}>Please Select</option>
                                                        <option value="Auction"{{ (old('authority') == 'Auction') ? ' selected="selected"' : '' }}>Auction</option>
                                                        <option value="Exclusive"{{ (old('authority') == 'Exclusive') ? ' selected="selected"' : '' }}>Exclusive</option>
                                                        <option value="Multi List"{{ (old('authority') == 'Multi List') ? ' selected="selected"' : '' }}>Multi List</option>
                                                        <option value="Conjunctional"{{ (old('authority') == 'Conjunctional') ? ' selected="selected"' : '' }}>Conjunctional</option>
                                                        <option value="Open"{{ (old('authority') == 'Open') ? ' selected="selected"' : '' }}>Open</option>
                                                        <option value="Sale by Negotiation"{{ (old('authority') == 'Sale by Negotiation') ? ' selected="selected"' : '' }}>Sale by Negotiation</option>
                                                        <option value="Set Sale"{{ (old('property_type') == 'Set Sale') ? ' selected="selected"' : '' }}>Set Sale</option>
                                                    </select>
                                                    @if ($errors->has('authority'))
                                                        <small class="help-block">{{ $errors->first('authority') }}</small>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ ($errors->has('price')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Price *</label>
                                                <div class="col-sm-10">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fas fa-dollar-sign"></i>
                                                        </div>
                                                        <input type="text" class="form-control" name="price" id="price" placeholder="Price" value="{{ old('price') }}">
                                                    </div>
                                                    @if ($errors->has('price'))
                                                        <small class="help-block">{{ $errors->first('price') }}</small>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Price Display</label>
                                                <div class="col-sm-10">

                                                    <label class="radio-label">
                                                        <input type="radio" name="price_display" class="minimal" value="Show actual price" {{ (old('price_display') == 'Show actual price' || old('price_display') == "") ? ' checked="checked"' : '' }}>
                                                        Show Actual price
                                                    </label><br />
                                                    <label class="radio-label">
                                                        <input type="radio" name="price_display" class="minimal" value="Show text instead of price" {{ (old('price_display') == 'Show text instead of price') ? ' checked="checked"' : '' }}>
                                                        Show text instead of price
                                                    </label><br />
                                                    <input type="text" class="form-control" name="price_text" id="price_text" value="{{ old('price_text') }}">
                                                    <label class="radio-label">
                                                        <input type="radio" name="price_display" class="minimal"  value="Hide the price" {{ (old('price_display') == 'Hide the price') ? ' checked="checked"' : '' }}>
                                                        Hide the price and display 'Contact Agent'
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group{{ ($errors->has('unit')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Unit</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control" name="unit" id="unit" placeholder="Unit" value="{{ old('unit') }}">
                                                    @if ($errors->has('unit'))
                                                        <small class="help-block">{{ $errors->first('unit') }}</small>
                                                    @endif
                                                </div>
                                            </div>


                                            <div class="form-group{{ ($errors->has('street_number')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Street Address *</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control" name="street_number" id="street_number" placeholder="Number" value="{{ old('street_number') }}">
                                                    @if ($errors->has('street_number'))
                                                        <small class="help-block">{{ $errors->first('street_number') }}</small>
                                                    @endif
                                                </div>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control" name="street_name" id="street_name" placeholder="Street Name" value="{{ old('street_name') }}">
                                                    @if ($errors->has('street_name'))
                                                        <small class="help-block">{{ $errors->first('street_name') }}</small>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-10">
                                                    <label class="cb-label">
                                                        <input type="checkbox" name="hide_street_address" class="minimal" value="Hide street address" {{ (old('hide_street_address') == 'Hide street address') ? ' checked="checked"' : '' }}> Hide street address on listing
                                                    </label><br />
                                                    <label class="cb-label">
                                                        <input type="checkbox" name="hide_street_view" class="minimal" value="Hide street view" {{ (old('hide_street_view') == 'Hide street view') ? ' checked="checked"' : '' }}> Hide street view
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group{{ ($errors->has('suburb')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Suburb *</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="suburb" id="suburb" placeholder="Suburb" value="{{ old('suburb') }}">
                                                    @if ($errors->has('suburb'))
                                                        <small class="help-block">{{ $errors->first('suburb') }}</small>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ ($errors->has('municipality')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Municipality</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="municipality" id="municipality" placeholder="Municipality" value="{{ old('municipality') }}">
                                                    @if ($errors->has('municipality'))
                                                        <small class="help-block">{{ $errors->first('municipality') }}</small>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <div class="btn-group pull-right">
                                                        <button type="button" class="btn btn-default tab-prev"><i class="fas fa-arrow-left"></i> Prev</button>
                                                        <button type="button" class="btn btn-default tab-next">Next <i class="fas fa-arrow-right"></i></button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div id="about-the-property" class="tab-pane">

                                            <div class="form-group{{ ($errors->has('bedrooms')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Bedrooms *</label>
                                                <div class="col-sm-4{{ ($errors->has('bedrooms')) ? ' has-error' : '' }}">
                                                    <select name="bedrooms" class="form-control select2" data-placeholder="All" style="width: 100%;">
                                                        <option value="0"{{ (old('bedrooms') == 'studio') ? ' selected="selected"' : '' }}>Studio</option>
                                                        @for($i=1; $i<=30; $i++)
                                                            <option value="{{ $i }}"{{ (old('bedrooms') == $i) ? ' selected="selected"' : '' }}>{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                    @if ($errors->has('bedrooms'))
                                                        <small class="help-block">{{ $errors->first('authority') }}</small>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ ($errors->has('bathrooms')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Bathrooms *</label>
                                                <div class="col-sm-4{{ ($errors->has('bathrooms')) ? ' has-error' : '' }}">
                                                    <select name="bathrooms" class="form-control select2" data-placeholder="All" style="width: 100%;">
                                                        @for($i=1; $i<=20; $i++)
                                                            <option value="{{ $i }}"{{ (old('bathrooms') == $i) ? ' selected="selected"' : '' }}>{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                    @if ($errors->has('bathrooms'))
                                                        <small class="help-block">{{ $errors->first('bathrooms') }}</small>
                                                    @endif
                                                </div>
                                                <div class="col-sm-4">including ensuites</div>
                                            </div>

                                            <div class="form-group{{ ($errors->has('ensuites')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Ensuites</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="ensuites" id="ensuites" placeholder="Ensuites" value="{{ old('ensuites') }}">
                                                    @if ($errors->has('ensuites'))
                                                        <small class="help-block">{{ $errors->first('ensuites') }}</small>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ ($errors->has('toilets')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Toilets</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="toilets" id="toilets" placeholder="Toilets" value="{{ old('toilets') }}">
                                                    @if ($errors->has('toilets'))
                                                        <small class="help-block">{{ $errors->first('toilets') }}</small>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ ($errors->has('garage_spaces')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Garage Spaces</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="garage_spaces" id="garage_spaces" placeholder="Garage Spaces" value="{{ old('garage_spaces') }}">
                                                    @if ($errors->has('garage_spaces'))
                                                        <small class="help-block">{{ $errors->first('garage_spaces') }}</small>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ ($errors->has('carport_spaces')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Carport Spaces</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="carport_spaces" id="carport_spaces" placeholder="Carport Spaces" value="{{ old('carport_spaces') }}">
                                                    @if ($errors->has('carport_spaces'))
                                                        <small class="help-block">{{ $errors->first('carport_spaces') }}</small>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ ($errors->has('open_spaces')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Open Spaces</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="open_spaces" id="open_spaces" placeholder="Open Spaces" value="{{ old('open_spaces') }}">
                                                    @if ($errors->has('open_spaces'))
                                                        <small class="help-block">{{ $errors->first('open_spaces') }}</small>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ ($errors->has('living_areas')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Living Areas</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="living_areas" id="living_areas" placeholder="Living Areas" value="{{ old('living_areas') }}">
                                                    @if ($errors->has('living_areas'))
                                                        <small class="help-block">{{ $errors->first('living_areas') }}</small>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ ($errors->has('house_size')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">House Size</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="house_size" id="house_size" placeholder="House Size" value="{{ old('house_size') }}">
                                                </div>

                                                <div class="col-sm-4">
                                                    <select name="house_size_unit" class="form-control select2" data-placeholder="All" style="width: 100%;">
                                                        <option value="Squares"{{ (old('house_size_unit') == 'Squares') ? ' selected="selected"' : '' }}>Squares</option>
                                                        <option value="Square Meters"{{ (old('house_size_unit') == 'Square Meters') ? ' selected="selected"' : '' }}>Square Meters</option>
                                                        <option value="Square Feet"{{ (old('house_size_unit') == 'Square Feet') ? ' selected="selected"' : '' }}>Square Feet</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group{{ ($errors->has('land_size')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Land Size</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="land_size" id="land_size" placeholder="Land Size" value="{{ old('land_size') }}">
                                                </div>

                                                <div class="col-sm-4">
                                                    <select name="land_size_unit" class="form-control select2" data-placeholder="All" style="width: 100%;">
                                                        <option value="Square Meters"{{ (old('land_size_unit') == 'Square Meters') ? ' selected="selected"' : '' }}>Square Meters</option>
                                                        <option value="Hectares"{{ (old('land_size_unit') == 'Hectares') ? ' selected="selected"' : '' }}>Hectares</option>
                                                        <option value="Square Feet"{{ (old('land_size_unit') == 'Square Feet') ? ' selected="selected"' : '' }}>Square Feet</option>
                                                        <option value="Acres"{{ (old('land_size_unit') == 'Acres') ? ' selected="selected"' : '' }}>Acres</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group{{ ($errors->has('enery_efficiency_rating')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Enery Efficiency Rating</label>
                                                <div class="col-sm-4{{ ($errors->has('enery_efficiency_rating')) ? ' has-error' : '' }}">
                                                    <select name="enery_efficiency_rating" class="form-control select2" data-placeholder="All" style="width: 100%;">
                                                        <option value="0"{{ (old('enery_efficiency_rating') == '0') ? ' selected="selected"' : '' }}>0</option>
                                                        <option value="0.5"{{ (old('enery_efficiency_rating') == '0.5') ? ' selected="selected"' : '' }}>0.5</option>
                                                        <option value="1"{{ (old('enery_efficiency_rating') == '1') ? ' selected="selected"' : '' }}>1</option>
                                                        <option value="1.5"{{ (old('enery_efficiency_rating') == '1.5') ? ' selected="selected"' : '' }}>1.5</option>
                                                        <option value="2"{{ (old('enery_efficiency_rating') == '2') ? ' selected="selected"' : '' }}>2</option>
                                                        <option value="2.5"{{ (old('enery_efficiency_rating') == '2.5') ? ' selected="selected"' : '' }}>2.5</option>
                                                        <option value="3"{{ (old('enery_efficiency_rating') == '3') ? ' selected="selected"' : '' }}>3</option>
                                                        <option value="3.5"{{ (old('enery_efficiency_rating') == '3.5') ? ' selected="selected"' : '' }}>3.5</option>
                                                        <option value="4"{{ (old('enery_efficiency_rating') == '4') ? ' selected="selected"' : '' }}>4</option>
                                                        <option value="4.5"{{ (old('enery_efficiency_rating') == '4.5') ? ' selected="selected"' : '' }}>4.5</option>
                                                        <option value="5"{{ (old('enery_efficiency_rating') == '5') ? ' selected="selected"' : '' }}>5</option>
                                                        <option value="5.5"{{ (old('enery_efficiency_rating') == '5.5') ? ' selected="selected"' : '' }}>5.5</option>
                                                        <option value="6"{{ (old('enery_efficiency_rating') == '6') ? ' selected="selected"' : '' }}>6</option>
                                                        <option value="6.5"{{ (old('enery_efficiency_rating') == '6.5') ? ' selected="selected"' : '' }}>6.5</option>
                                                        <option value="7"{{ (old('enery_efficiency_rating') == '7') ? ' selected="selected"' : '' }}>7</option>
                                                        <option value="7.5"{{ (old('enery_efficiency_rating') == '7.5') ? ' selected="selected"' : '' }}>7.5</option>
                                                        <option value="8"{{ (old('enery_efficiency_rating') == '8') ? ' selected="selected"' : '' }}>8</option>
                                                        <option value="8.5"{{ (old('enery_efficiency_rating') == '8.5') ? ' selected="selected"' : '' }}>8.5</option>
                                                        <option value="9"{{ (old('enery_efficiency_rating') == '9') ? ' selected="selected"' : '' }}>9</option>
                                                        <option value="9.5"{{ (old('enery_efficiency_rating') == '9.5') ? ' selected="selected"' : '' }}>9.5</option>
                                                        <option value="10"{{ (old('enery_efficiency_rating') == '10') ? ' selected="selected"' : '' }}>10</option>
                                                    </select>
                                                    @if ($errors->has('enery_efficiency_rating'))
                                                        <small class="help-block">{{ $errors->first('enery_efficiency_rating') }}</small>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Outdoor Features</label>
                                                <div class="col-sm-10 features_container">
                                                    <div class="col-sm-6">
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="outdoor_features[]" class="minimal" value="Balcony" {{ (is_array(old('outdoor_features')) && in_array('Balcony', old('outdoor_features'))) ? "checked" : "" }}> Balcony
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="outdoor_features[]" class="minimal" value="Deck" {{ (is_array(old('outdoor_features')) && in_array('Deck', old('outdoor_features'))) ? "checked" : "" }}> Deck
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="outdoor_features[]" class="minimal" value="Outdoor Entertainment Area" {{ (is_array(old('outdoor_features')) && in_array('Outdoor Entertainment Area', old('outdoor_features'))) ? "checked" : "" }}> Outdoor Entertainment Area
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="outdoor_features[]" class="minimal" value="Remote Garage" {{ (is_array(old('outdoor_features')) && in_array('Remote Garage', old('outdoor_features'))) ? "checked" : "" }}> Remote Garage
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="outdoor_features[]" class="minimal" value="Shed" {{ (is_array(old('outdoor_features')) && in_array('Shed', old('outdoor_features'))) ? "checked" : "" }}> Shed
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="outdoor_features[]" class="minimal" value="Swimming Pool - In Ground" {{ (is_array(old('outdoor_features')) && in_array('Swimming Pool - In Ground', old('outdoor_features'))) ? "checked" : "" }}> Swimming Pool - In Ground
                                                        </label>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="outdoor_features[]" class="minimal" value="Courtyard" {{ (is_array(old('outdoor_features')) && in_array('Courtyard', old('outdoor_features'))) ? "checked" : "" }}> Courtyard
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="outdoor_features[]" class="minimal" value="Fully Fenced" {{ (is_array(old('outdoor_features')) && in_array('Fully Fenced', old('outdoor_features'))) ? "checked" : "" }}> Fully Fenced
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="outdoor_features[]" class="minimal" value="Outside Spa" {{ (is_array(old('outdoor_features')) && in_array('Outside Spa', old('outdoor_features'))) ? "checked" : "" }}> Outside Spa
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="outdoor_features[]" class="minimal" value="Secure Parking" {{ (is_array(old('outdoor_features')) && in_array('Secure Parking', old('outdoor_features'))) ? "checked" : "" }}> Secure Parking
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="outdoor_features[]" class="minimal" value="Swimming Pool - Above Ground" {{ (is_array(old('outdoor_features')) && in_array('Swimming Pool - Above Ground', old('outdoor_features'))) ? "checked" : "" }}> Swimming Pool - Above Ground
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="outdoor_features[]" class="minimal" value="Tennis Court" {{ (is_array(old('outdoor_features')) && in_array('Tennis Court', old('outdoor_features'))) ? "checked" : "" }}> Tennis Court
                                                        </label>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Indoor Features</label>
                                                <div class="col-sm-10 features_container">

                                                    <div class="col-sm-6">
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="indoor_features[]" class="minimal" value="Alarm System" {{ (is_array(old('indoor_features')) && in_array('Alarm System', old('indoor_features'))) ? "checked" : "" }}> Alarm System
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="indoor_features[]" class="minimal" value="Built-in Wardrobes" {{ (is_array(old('indoor_features')) && in_array('Built-in Wardrobes', old('indoor_features'))) ? "checked" : "" }}> Built-in Wardrobes
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="indoor_features[]" class="minimal" value="Ducted Vacuum System" {{ (is_array(old('indoor_features')) && in_array('Ducted Vacuum System', old('indoor_features'))) ? "checked" : "" }}> Ducted Vacuum System
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="indoor_features[]" class="minimal" value="Gym" {{ (is_array(old('indoor_features')) && in_array('Gym', old('indoor_features'))) ? "checked" : "" }}> Gym
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="indoor_features[]" class="minimal" value="Intercom" {{ (is_array(old('indoor_features')) && in_array('Intercom', old('indoor_features'))) ? "checked" : "" }}> Intercom
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="indoor_features[]" class="minimal" value="Rumpus Room" {{ (is_array(old('indoor_features')) && in_array('Rumpus Room', old('indoor_features'))) ? "checked" : "" }}> Rumpus Room
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="indoor_features[]" class="minimal" value="Workshop" {{ (is_array(old('indoor_features')) && in_array('Workshop', old('indoor_features'))) ? "checked" : "" }}> Workshop
                                                        </label>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="indoor_features[]" class="minimal" value="Broadband Internet Available" {{ (is_array(old('indoor_features')) && in_array('Broadband Internet Available', old('indoor_features'))) ? "checked" : "" }}> Broadband Internet Available
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="indoor_features[]" class="minimal" value="Dishwasher" {{ (is_array(old('indoor_features')) && in_array('Dishwasher', old('indoor_features'))) ? "checked" : "" }}> Dishwasher
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="indoor_features[]" class="minimal" value="Floorboards" {{ (is_array(old('indoor_features')) && in_array('Floorboards', old('indoor_features'))) ? "checked" : "" }}> Floorboards
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="indoor_features[]" class="minimal" value="Inside Spa" {{ (is_array(old('indoor_features')) && in_array('Inside Spa', old('indoor_features'))) ? "checked" : "" }}> Inside Spa
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="indoor_features[]" class="minimal" value="Pay TV Access" {{ (is_array(old('indoor_features')) && in_array('Pay TV Access', old('indoor_features'))) ? "checked" : "" }}> Pay TV Access
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="indoor_features[]" class="minimal" value="Study" {{ (is_array(old('indoor_features')) && in_array('Study', old('indoor_features'))) ? "checked" : "" }}> Study
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Heating / Cooling</label>
                                                <div class="col-sm-10 features_container">

                                                    <div class="col-sm-6">
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="heating_cooling[]" class="minimal" value="Air Conditioning" {{ (is_array(old('heating_cooling')) && in_array('Air Conditioning', old('heating_cooling'))) ? "checked" : "" }}> Air Conditioning
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="heating_cooling[]" class="minimal" value="Ducted Heating" {{ (is_array(old('heating_cooling')) && in_array('Ducted Heating', old('heating_cooling'))) ? "checked" : "" }}> Ducted Heating
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="heating_cooling[]" class="minimal" value="Gas Heating" {{ (is_array(old('heating_cooling')) && in_array('Gas Heating', old('heating_cooling'))) ? "checked" : "" }}> Gas Heating
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="heating_cooling[]" class="minimal" value="Open Fireplace" {{ (is_array(old('heating_cooling')) && in_array('Open Fireplace', old('heating_cooling'))) ? "checked" : "" }}> Open Fireplace
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="heating_cooling[]" class="minimal" value="Split-System Air Conditioning" {{ (is_array(old('heating_cooling')) && in_array('Split-System Air Conditioning', old('heating_cooling'))) ? "checked" : "" }}> Split-System Air Conditioning
                                                        </label>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="heating_cooling[]" class="minimal" value="Ducted Cooling" {{ (is_array(old('heating_cooling')) && in_array('Ducted Cooling', old('heating_cooling'))) ? "checked" : "" }}> Ducted Cooling
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="heating_cooling[]" class="minimal" value="Evaporative Cooling" {{ (is_array(old('heating_cooling')) && in_array('Evaporative Cooling', old('heating_cooling'))) ? "checked" : "" }}> Evaporative Cooling
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="heating_cooling[]" class="minimal" value="Hydronic Heating" {{ (is_array(old('heating_cooling')) && in_array('Hydronic Heating', old('heating_cooling'))) ? "checked" : "" }}> Hydronic Heating
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="heating_cooling[]" class="minimal" value="Reverse Cycle Air Conditioning" {{ (is_array(old('heating_cooling')) && in_array('Reverse Cycle Air Conditioning', old('heating_cooling'))) ? "checked" : "" }}> Reverse Cycle Air Conditioning
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="heating_cooling[]" class="minimal" value="Split-System Heating" {{ (is_array(old('heating_cooling')) && in_array('Split-System Heating', old('heating_cooling'))) ? "checked" : "" }}> Split-System Heating
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Eco Friendly Features</label>
                                                <div class="col-sm-10 features_container">

                                                    <div class="col-sm-6">
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="eco_friendly_features[]" class="minimal" value="Grey Water System" {{ (is_array(old('eco_friendly_features')) && in_array('Grey Water System', old('eco_friendly_features'))) ? "checked" : "" }}> Grey Water System
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="eco_friendly_features[]" class="minimal" value="Solar Panels" {{ (is_array(old('eco_friendly_features')) && in_array('Solar Panels', old('eco_friendly_features'))) ? "checked" : "" }}> Solar Panels
                                                        </label>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="eco_friendly_features[]" class="minimal" value="Solar Hot Water" {{ (is_array(old('eco_friendly_features')) && in_array('Solar Hot Water', old('eco_friendly_features'))) ? "checked" : "" }}> Solar Hot Water
                                                        </label><br />
                                                        <label class="cb-label">
                                                            <input type="checkbox" name="eco_friendly_features[]" class="minimal" value="Water Tank" {{ (is_array(old('eco_friendly_features')) && in_array('Water Tank', old('eco_friendly_features'))) ? "checked" : "" }}> Water Tank
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group{{ ($errors->has('other_features')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Other Features</label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="other_features" id="other_features" placeholder="Other Features" value="{{ old('other_features') }}">
                                                    @if ($errors->has('other_features'))
                                                        <small class="help-block">{{ $errors->first('other_features') }}</small>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <div class="btn-group pull-right">
                                                        <button type="button" class="btn btn-default tab-prev"><i class="fas fa-arrow-left"></i> Prev</button>
                                                        <button type="button" class="btn btn-default tab-next">Next <i class="fas fa-arrow-right"></i></button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div id="listing-copy-and-images" class="tab-pane">

                                            <div class="form-group {{ ($errors->has('headline')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Headline *</label>

                                                <div class="col-sm-10">
                                                    <textarea class="form-control" rows="3" name="headline" placeholder="Headline">{{ old('headline') }}</textarea>
                                                    @if ($errors->has('headline'))
                                                        <small class="help-block">{{ $errors->first('headline') }}</small>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ ($errors->has('description')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Description *</label>

                                                <div class="col-sm-10">
                                                    <textarea id="description" name="description" rows="10" cols="80" style="height: 500px;">{{ old('description') }}</textarea>
                                                    @if ($errors->has('description'))
                                                        <small class="help-block">{{ $errors->first('description') }}</small>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group {{ ($errors->has('thumbnail')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Images</label>
                                                <div class="col-sm-10">
                                                    <input type="hidden" id="imageCount" name="imageCount" value="{{ (old('imageCount') != "" ? old('imageCount') : 0) }}">

                                                    <button id="image-popup" type="button" class="btn btn-info btn-sm">Upload Images</button>
                                                    @php
                                                        $class = ' invisible';
                                                        if(old('imageCount') && old('imageCount') > 0){
                                                            $class = '';
                                                        }
                                                        $i=0;
                                                    @endphp
                                                    <br/><br/>
                                                    <span id="added_image">
                                                    @foreach($images as $image)
                                                        <div id="imgBox{{ $i }}" class="image-box sortable">
                                                            <input id="imgPosition{{ $i }}" name="imgPosition{{ $i }}" class="imgPosition" type="hidden" value="{{ $image->position }}">
                                                            <input id="imgDelete{{ $i }}" name="imgDelete{{ $i }}" type="hidden" value="{{ $image->delete }}">
                                                            <input id="imgType{{ $i }}" name="imgType{{ $i }}" type="hidden" value="new">

                                                            <div class="image-box-toolbox">

                                                                <a class="tool" title="Delete" href="#" onclick="image_delete({{ $i }}); return false;"><i class="fa fa-trash-alt"></i></a>

                                                                <div class="pull-right" style="margin-right: 10px">
                                                                    <div style="width: 33px; height: 22px;">
                                                                        @php
                                                                        $status = 'active';
                                                                        if(old('imgStatus' . $i)=='on'){
                                                                            $status = 'active';
                                                                        }else{
                                                                            $status = '';
                                                                        }
                                                                        @endphp
                                                                        <input id="imgStatus{{ $i }}" name="imgStatus{{ $i }}" data-id="{{ $i }}" type="checkbox" class="image_status" data-size="mini" {{ ($image->status == 'active' || $image->status == 'on') ? ' checked' : null }}>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                            <div class="image-box-img">
                                                                <image class="image-item" src="{{ old('imgLocation' . $i)}}"/>
                                                                <input id="imgLocation{{ $i }}" name="imgLocation{{ $i }}" type="hidden" value="{{ old('imgLocation' . $i)}}">
                                                            </div>

                                                        </div>
                                                        @php
                                                        $i++;
                                                        @endphp
                                                    @endforeach
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="form-group {{ ($errors->has('floorplan')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Floorplans</label>
                                                <div class="col-sm-10">
                                                    <input type="hidden" id="floorplanCount" name="floorplanCount" value="{{ (old('floorplanCount') != "" ? old('floorplanCount') : 0) }}">

                                                    <button id="floorplan-popup" type="button" class="btn btn-info btn-sm">Upload Floorplan</button>
                                                    @php
                                                        $class = ' invisible';
                                                        if(old('floorplanCount') && old('floorplanCount') > 0){
                                                            $class = '';
                                                        }
                                                        $i=0;
                                                    @endphp
                                                    <br/><br/>
                                                    <span id="added_floorplan">
                                                    @foreach($floorplans as $floorplan)
                                                            <div id="flrBox{{ $i }}" class="floorplan-box sortable">
                                                            <input id="flrPosition{{ $i }}" name="flrPosition{{ $i }}" class="flrPosition" type="hidden" value="{{ $floorplan->position }}">
                                                            <input id="flrDelete{{ $i }}" name="flrDelete{{ $i }}" type="hidden" value="{{ $floorplan->delete }}">
                                                            <input id="flrType{{ $i }}" name="flrType{{ $i }}" type="hidden" value="new">

                                                            <div class="floorplan-box-toolbox">

                                                                <a class="tool" title="Delete" href="#" onclick="floorplan_delete({{ $i }}); return false;"><i class="fa fa-trash-alt"></i></a>

                                                                <div class="pull-right" style="margin-right: 10px">
                                                                    <div style="width: 33px; height: 22px;">
                                                                        @php
                                                                            $status = 'active';
                                                                            if(old('flrStatus' . $i)=='on'){
                                                                                $status = 'active';
                                                                            }else{
                                                                                $status = '';
                                                                            }
                                                                        @endphp
                                                                        <input id="flrStatus{{ $i }}" name="flrStatus{{ $i }}" data-id="{{ $i }}" type="checkbox" class="floorplan_status" data-size="mini" {{ ($floorplan->status == 'active' || $floorplan->status == 'on') ? ' checked' : null }}>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                            <div class="floorplan-box-img">
                                                                <image class="floorplan-item" src="{{ old('flrLocation' . $i)}}"/>
                                                                <input id="flrLocation{{ $i }}" name="flrLocation{{ $i }}" type="hidden" value="{{ old('flrLocation' . $i)}}">
                                                            </div>

                                                        </div>
                                                            @php
                                                                $i++;
                                                            @endphp
                                                        @endforeach
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="form-group {{ ($errors->has('statement_of_information_pdf')) ? ' has-error' : '' }}">
                                                <label class="col-sm-2 control-label">Statement of Information </label>
                                                <div class="col-sm-10">
                                                    <input type="hidden" id="statement_of_information_pdf" name="statement_of_information_pdf" value="{{ old('statement_of_information_pdf') }}">
                                                    <button id="document-popup" type="button" class="btn btn-info btn-sm">Upload Document</button>
                                                    @php
                                                    $class = ' invisible';
                                                    if(old('statement_of_information_pdf')){
                                                        $class = '';
                                                    }
                                                    @endphp
                                                    <button id="remove-document" type="button" class="btn btn-danger btn-sm{{ $class }}">Remove Document</button>
                                                    <br/><br/>
                                                    <span id="added_document">
                                                    @if(old('statement_of_information_pdf'))
                                                        {{ url('dreamcms/').old('statement_of_information_pdf') }}
                                                    @endif
                                                    </span>
                                                    @if ($errors->has('statement_of_information_pdf'))
                                                        <small class="help-block">{{ $errors->first('statement_of_information_pdf') }}</small>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <div class="btn-group pull-right">
                                                        <button type="button" class="btn btn-default tab-prev"><i class="fas fa-arrow-left"></i> Prev</button>
                                                        <button type="button" class="btn btn-default tab-next">Next <i class="fas fa-arrow-right"></i></button>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div id="create-inspection-times" class="tab-pane">

                                            <div class="form-group{{ ($errors->has('inspection_times')) ? ' has-error' : '' }}">

                                                <input type="hidden" id="inspections_data" name="inspections_data" value='{{ old('inspections_data','[]') }}'>

                                                <div class="col-sm-3">
                                                    <div class="input-group date">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                        <input type="text" id="inspection_date" name="inspection_date" class="form-control pull-right datepicker" placeholder="Date" value="{{ date("d/m/Y") }}">
                                                    </div>
                                                </div>

                                                <div class="col-sm-3 bootstrap-timepicker">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fas fa-play-circle"></i>
                                                        </div>
                                                        <input type="text" id="inspection_start_time" name="inspection_start_time" class="form-control timepicker">
                                                    </div>
                                                </div>

                                                <div class="col-sm-3 bootstrap-timepicker">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <i class="fas fa-stop-circle"></i>
                                                        </div>
                                                        <input type="text" id="inspection_end_time" name="inspection_end_time" class="form-control timepicker">
                                                    </div>
                                                </div>

                                                <div class="col-sm-3">
                                                    <button id="add-inspection" class="btn btn-info btn-sm add-ins-button" type="button">Add
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>

                                                <div class="col-sm-12" id="inspection_table_container"></div>

                                                @if ($errors->has('inspection_times'))
                                                    <small class="help-block">{{ $errors->first('statement_of_information_pdf') }}</small>
                                                @endif
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button type="button" class="btn btn-default tab-prev pull-right"><i class="fas fa-arrow-left"></i> Prev</button>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="box-footer">
                                <a href="{{ url('dreamcms/news') }}" class="btn btn-info pull-right">Cancel</a>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save_close">Save & Close</button>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save">Save</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </section>

    </div>

    <div class="modal fade" id="change-slug">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Change SEO Name</h4>
                </div>
                <div class="modal-body">
                    <input type="text" class="form-control" id="slug-modal" name="slug-modal" value="{{ old('slug') }}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" id="save-seo" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('/components/ckfinder/ckfinder.js') }}"></script>
    <script src="{{ asset('/components/theme/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('/components/jquery-mask-plugin/dist/jquery.mask.js') }}"></script>
    <script src="{{ asset('/components/theme/plugins/timepicker/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('/components/theme/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
    <script src="{{ asset('/js/admin/number-to-words.js') }}"></script>
    <script src="{{ asset('/js/admin/properties/properties.js') }}"></script>
    <script src="{{ asset('/js/admin/properties/images.js') }}"></script>
    <script src="{{ asset('/js/admin/properties/floorplans.js') }}"></script>
    <script src="{{ asset('/js/admin/properties/statement.js') }}"></script>
    <script src="{{ asset('/js/admin/properties/inspections.js') }}"></script>
@endsection
@section('inline-scripts')
<script type="text/javascript">
@if(old('inspections_data')!='')
    buildTable();
@endif
</script>
@endsection