<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">

            <!-- Contact Module -->
            @if($contact_status=='active')
                @can('view-contact')
                    <li class="treeview{!! Request::segment(2) == 'contact' ? ' menu-open active' : null !!}">
                        <a href="#">
                            <i class="fa fa-envelope"></i> <span>{{ $contact_display_name }}</span>
                            <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                        </a>
                        <ul class="treeview-menu">
                            <li {!! (Request::segment(2) == 'form-builder') && (Request::segment(3) == '') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/contact/form-builder') }}"><i class="fas fa-circle"></i> Form Builder</a></li>
                            <li {!! (Request::segment(2) == 'contact') && (Request::segment(3) == '') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/contact') }}"><i class="fas fa-circle"></i> Inbox</a></li>
                            @can('view-edit-details')
                            <li {!! Request::segment(3) == 'details' ? ' class="active"' : null !!}><a href="{{ url('dreamcms/contact/details') }}"><i class="fas fa-circle"></i> Details</a></li>
                            @endcan
                        </ul>
                    </li>
                @endcan
            @endif
                       
            <!-- Documents Module -->
            @if($documents_status=='active')
                @can('view-documents')
                    <li class="treeview{!! Request::segment(2) == 'documents' ? ' menu-open active' : null !!}">
                        <a href="#">
                            <i class="fas fa-sticky-note"></i> <span>{{ $documents_display_name }}</span>
                            <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
                        </a>
                        <ul class="treeview-menu">
                            <li {!! Request::segment(3) == 'categories' ? ' class="active"' : null !!}><a href="{{ url('dreamcms/documents/categories') }}"><i class="fas fa-circle"></i> Categories</a></li>
                            <li {!! (Request::segment(2) == 'documents') && (Request::segment(3) == '') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/documents') }}"><i class="fas fa-circle"></i> All Items</a></li>
                        </ul>
                    </li>
                @endcan
            @endif

            <!-- FAQs Module -->
            @if($faqs_status=='active')
                @can('view-faqs')
                    <li class="treeview{!! Request::segment(2) == 'faqs' ? ' menu-open active' : null !!}">
                        <a href="#">
                            <i class="fa fa-question-circle"></i> <span>{{ $faqs_display_name }}</span>
                            <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                        </a>
                        <ul class="treeview-menu">
                            <li {!! Request::segment(3) == 'categories' ? ' class="active"' : null !!}><a href="{{ url('dreamcms/faqs/categories') }}"><i class="fas fa-circle"></i> Categories</a></li>
                            <li {!! (Request::segment(2) == 'faqs') && (Request::segment(3) == '') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/faqs') }}"><i class="fas fa-circle"></i> All Items</a></li>
                        </ul>
                    </li>
                @endcan
            @endif

            <!-- Gallery Module -->
            @if($gallery_status=='active')
                @can('view-gallery')
                    <li class="treeview{!! Request::segment(2) == 'gallery' ? ' menu-open active' : null !!}">
                        <a href="#">
                            <i class="fas fa-images"></i> <span>{{ $gallery_display_name }}</span>
                            <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                        </a>
                        <ul class="treeview-menu">
                            <li {!! Request::segment(3) == 'categories' ? ' class="active"' : null !!}><a href="{{ url('dreamcms/gallery/categories') }}"><i class="fas fa-circle"></i> Categories</a></li>
                            <li {!! (Request::segment(2) == 'gallery') && (Request::segment(3) == '') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/gallery') }}"><i class="fas fa-circle"></i> All Items</a></li>
                        </ul>
                    </li>
                @endcan
            @endif
            
            <!-- Links Module -->
            @if($links_status=='active')
            @can('view-links')
            <li class="treeview{!! Request::segment(2) == 'links' ? ' menu-open active' : null !!}">
                <a href="#">
                    <i class="fas fa-link"></i> <span>{{ $links_display_name }}</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li {!! Request::segment(3) == 'categories' ? ' class="active"' : null !!}><a href="{{ url('dreamcms/links/categories') }}"><i class="fas fa-circle"></i> Categories</a></li>
                    <li {!! (Request::segment(2) == 'links') && (Request::segment(3) == '') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/links') }}"><i class="fas fa-circle"></i> All Items</a></li>
                </ul>
            </li>
            @endcan
            @endif

            <!-- Members Module -->
            @if($members_status=='active')
                @can('view-members')
                    <li class="treeview{!! Request::segment(2) == 'members' ? ' menu-open active' : null !!}">
                        <a href="#">
                            <i class="fa fa-users"></i> <span>{{ $members_display_name }}</span>
                            <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                        </a>
                        <ul class="treeview-menu">
                            <li {!! Request::segment(3) == 'types' ? ' class="active"' : null !!}><a href="{{ url('dreamcms/members/types') }}"><i class="fas fa-circle"></i> Types</a></li>
                            <li {!! (Request::segment(2) == 'members') && (Request::segment(3) == '') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/members') }}"><i class="fas fa-circle"></i> All Items</a></li>
                        </ul>
                    </li>
                @endcan
            @endif

            <!-- News Module -->
            @if($news_status=='active')
                @can('view-news')
                    <li class="treeview{!! Request::segment(2) == 'news' ? ' menu-open active' : null !!}">
                        <a href="#">
                            <i class="far fa-newspaper"></i> <span>{{ $news_display_name }}</span>
                            <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                        </a>
                        <ul class="treeview-menu">
                            <li {!! Request::segment(3) == 'categories' ? ' class="active"' : null !!}><a href="{{ url('dreamcms/news/categories') }}"><i class="fas fa-circle"></i> Categories</a></li>
                            <li {!! (Request::segment(2) == 'news') && (Request::segment(3) == '') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/news') }}"><i class="fas fa-circle"></i> All Items</a></li>
                        </ul>
                    </li>
                @endcan
            @endif
            
            <!-- Orders Module -->
            @if($orders_status=='active')
                @can('view-orders')
                    <li class="treeview{!! Request::segment(2) == 'orders' ? ' menu-open active' : null !!}">
                        <a href="#">
                            <i class="fas fa-shopping-cart"></i> <span>{{ $orders_display_name }}</span>
                            <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                        </a>
                        <ul class="treeview-menu">                           
                            <li {!! (Request::segment(2) == 'orders') && (Request::segment(3) == '') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/orders') }}"><i class="fas fa-circle"></i> All Items</a></li>
                        </ul>
                    </li>
                @endcan
            @endif

            <!-- Pages Module -->
            @if($pages_status=='active')
            @can('view-pages')
            <li class="treeview{!! Request::segment(2) == 'pages' ? ' menu-open active' : null !!}">
                <a href="#">
                    <i class="fas fa-file-alt"></i> <span>{{ $pages_display_name }}</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li {!! Request::segment(3) == 'categories' ? ' class="active"' : null !!}><a href="{{ url('dreamcms/pages/categories') }}"><i class="fas fa-circle"></i> Categories</a></li>
                    <li {!! (Request::segment(2) == 'pages') && (Request::segment(3) == '') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/pages') }}"><i class="fas fa-circle"></i> All Items</a></li>
                </ul>
            </li>
            @endcan
            @endif
            
            <!-- Products Module -->
            @if($products_status=='active')
                @can('view-products')
                    <li class="treeview{!! Request::segment(2) == 'products' ? ' menu-open active' : null !!}">
                        <a href="#">
                            <i class="fas fa-gift"></i> <span>{{ $products_display_name }}</span>
                            <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                        </a>
                        <ul class="treeview-menu">
                            <li {!! Request::segment(3) == 'categories' ? ' class="active"' : null !!}><a href="{{ url('dreamcms/products/categories') }}"><i class="fas fa-circle"></i> Categories</a></li>
                            <li {!! (Request::segment(2) == 'products') && (Request::segment(3) == '') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/products') }}"><i class="fas fa-circle"></i> All Items</a></li>
                        </ul>
                    </li>
                @endcan
            @endif
            
            <!-- Projects Module -->
            @if($projects_status=='active')
                @can('view-projects')
                    <li class="treeview{!! Request::segment(2) == 'projects' ? ' menu-open active' : null !!}">
                        <a href="#">
                            <i class="fa fa-clipboard"></i> <span>{{ $projects_display_name }}</span>
                            <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                        </a>
                        <ul class="treeview-menu">
                            <li {!! Request::segment(3) == 'categories' ? ' class="active"' : null !!}><a href="{{ url('dreamcms/projects/categories') }}"><i class="fas fa-circle"></i> Categories</a></li>
                            <li {!! (Request::segment(2) == 'projects') && (Request::segment(3) == '') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/projects') }}"><i class="fas fa-circle"></i> All Items</a></li>
                        </ul>
                    </li>
                @endcan
            @endif        

            <!-- Team Module -->
            @if($team_status=='active')
                @can('view-team')
                    <li class="treeview{!! Request::segment(2) == 'team' ? ' menu-open active' : null !!}">
                        <a href="#">
                            <i class="fas fa-user-tie"></i> <span>{{ $team_display_name }}</span>
                            <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                        </a>
                        <ul class="treeview-menu">
                            <li {!! Request::segment(3) == 'categories' ? ' class="active"' : null !!}><a href="{{ url('dreamcms/team/categories') }}"><i class="fas fa-circle"></i> Categories</a></li>
                            <li {!! (Request::segment(2) == 'team') && (Request::segment(3) == '') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/team') }}"><i class="fas fa-circle"></i> All Items</a></li>
                        </ul>
                    </li>
                @endcan
            @endif

            <!-- Testimonials Module -->
            @if($testimonials_status=='active')
                @can('view-testimonials')
                    <li class="treeview{!! Request::segment(2) == 'testimonials' ? ' menu-open active' : null !!}">
                        <a href="#">
                            <i class="far fa-comments"></i> <span>{{ $testimonials_display_name }}</span>
                            <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                        </a>
                        <ul class="treeview-menu">
                            <li {!! (Request::segment(2) == 'testimonials') && (Request::segment(3) == '') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/testimonials') }}"><i class="fas fa-circle"></i> All Items</a></li>
                        </ul>
                    </li>
                @endcan
            @endif

            <li class="header"></li>

            @if(Auth::user()->role=='admin')
            <li {!! Request::segment(2) == 'modules' ? 'class="active"' : null !!}><a href="{{ url('dreamcms/modules') }}"><i class="fa fa-cubes"></i> <span>Modules</span></a></li>
            @endif

            <li {!! Request::segment(2) == 'user' ? 'class="active"' : null !!}><a href="{{ url('dreamcms/user') }}"><i class="fa fa-user"></i> <span>Users</span></a></li>

            @can('view-settings')
            <li class="treeview{!! Request::segment(2) == 'settings' ? ' menu-open active' : null !!}">
                <a href="#">
                    <i class="fa fa-cog"></i> <span>Settings</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @can('edit-general')
                    <li {!! (Request::segment(2) == 'settings') && (Request::segment(3) == '') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/settings') }}"><i class="fas fa-circle"></i> General</a></li>
                    @endcan
                    @can('edit-home-page')
                    <li {!! (Request::segment(2) == 'settings') && (Request::segment(3) == 'home-page') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/settings/home-page') }}"><i class="fas fa-circle"></i> Home Page</a></li>
                    @endcan
                    @can('edit-header-images')
                    <li {!! (Request::segment(2) == 'settings') && (Request::segment(3) == 'header-images') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/settings/header-images') }}"><i class="fas fa-circle"></i> Header Images</a></li>
                    @endcan
                    @can('edit-contact-details')
                    <li {!! (Request::segment(2) == 'settings') && (Request::segment(3) == 'contacts') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/settings/contacts') }}"><i class="fas fa-circle"></i> Contact Details</a></li>
                    @endcan
                    @can('edit-navigation')
                    <li {!! (Request::segment(2) == 'settings') && (Request::segment(3) == 'navigation') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/settings/navigation') }}"><i class="fas fa-circle"></i> Navigation</a></li>
                    @endcan
                    @can('edit-home-page-slider')
                    <li {!! (Request::segment(2) == 'settings') && (Request::segment(3) == 'home-sliders') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/settings/home-sliders') }}"><i class="fas fa-circle"></i> Home Page Slider</a></li>
                    @endcan
                    @can('edit-social-media')
                    <li {!! (Request::segment(2) == 'settings') && (Request::segment(3) == 'social-media') ? ' class="active"' : null !!}><a href="{{ url('dreamcms/settings/social-media') }}"><i class="fas fa-circle"></i> Social Media</a></li>
                    @endcan
                </ul>
            </li>
            @endcan
            
            <li {!! Request::segment(2) == 'media-library' ? 'class="active"' : null !!}><a href="{{ url('dreamcms/media-library') }}"><i class="fas fa-file"></i> <span>Media Library</span></a></li>

        </ul>
    </section>
</aside>