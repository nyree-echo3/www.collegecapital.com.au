@extends('admin.layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/iCheck/all.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Home Page</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home Page</a></li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Home Page Values</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/settings/home-page-update') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="box-body">

                                <div class="form-group {{ ($errors->has('meta_title')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Meta Title</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="meta_title"
                                               placeholder="Meta Title" value="{{ $meta_title->value }}">
                                        @if ($errors->has('meta_title'))
                                            <small class="help-block">{{ $errors->first('meta_title') }}</small>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ ($errors->has('meta_keywords')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Meta Keywords</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" rows="3" name="meta_keywords" placeholder="Meta Keywords">{{ $meta_keywords->value }}</textarea>
                                        @if ($errors->has('meta_keyword'))
                                            <small class="help-block">{{ $errors->first('meta_keywords') }}</small>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ ($errors->has('meta_description')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Meta Description</label>

                                    <div class="col-sm-10">

                                        <textarea class="form-control" rows="3" name="meta_description" placeholder="Meta Description">{{ $meta_description->value }}</textarea>
                                        @if ($errors->has('meta_description'))
                                            <small class="help-block">{{ $errors->first('meta_description') }}</small>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group {{ ($errors->has('home_intro_text')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Intro Text</label>

                                    <div class="col-sm-10">
                                        <textarea id="home_intro_text" name="home_intro_text" rows="10" cols="80"
                                                  style="height: 500px;">{{ $home_intro_text->value }}</textarea>
                                        @if ($errors->has('home_intro_text'))
                                            <small class="help-block">{{ $errors->first('home_intro_text') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('/components/theme/plugins/iCheck/icheck.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            CKEDITOR.replace('home_intro_text');
        });
    </script>
@endsection