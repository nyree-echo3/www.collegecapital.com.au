@extends('admin/layouts/app')

@section('styles')
<link rel="stylesheet" href="{{ asset('/components/jquery-ui/themes/ui-darkness/jquery-ui.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/members') }}"><i class="fa fa-users"></i> {{ $display_name }}</a></li>
            </ol>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Sort</h3>
                </div>
                <div class="box-body">

                    <table class="table table-striped table-hover">
                        <tbody class="sortable" data-entityname="members">
                        @foreach ($members as $member)
                        <tr data-itemId="{{ $member->id }}">
                            <td class="sortable-handle"><span class="glyphicon glyphicon-sort"></span></td>
                            <td class="id-column">{{ $member->id }}</td>
                            <td>{{ $member->title }}</td>
                        </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/jquery-ui/jquery-ui.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        var changePosition = function(requestData){
        $.ajax({
            'url': '{{ url('dreamcms/sort') }}',
            'type': 'POST',
            'data': requestData,
            'success': function(data) {
                if (data.success) {
                    toastr.options = {"closeButton": true}
                    toastr.success('Saved');
                } else {
                    toastr.options = {"closeButton": true}
                    toastr.error(data.errors);
                }
            },
            'error': function(){
                toastr.options = {"closeButton": true}
                toastr.error('Something wrong');
            }
        });
    };

    $(document).ready(function(){
        var $sortableTable = $('.sortable');
        if ($sortableTable.length > 0) {
            $sortableTable.sortable({
                handle: '.sortable-handle',
                axis: 'y',
                update: function(a, b){

                    var entityName = $(this).data('entityname');
                    var $sorted = b.item;

                    var $previous = $sorted.prev();
                    var $next = $sorted.next();

                    if ($previous.length > 0) {
                        changePosition({
                            parentId: $sorted.data('parentid'),
                            type: 'moveBefore',
                            entityName: entityName,
                            id: $sorted.data('itemid'),
                            positionEntityId: $previous.data('itemid')
                        });
                    } else if ($next.length > 0) {
                        changePosition({
                            parentId: $sorted.data('parentid'),
                            type: 'moveAfter',
                            entityName: entityName,
                            id: $sorted.data('itemid'),
                            positionEntityId: $next.data('itemid')
                        });
                    } else {
                        console.error('Something wrong!');
                    }
                },
                cursor: "move"
            });
        }
    });
    </script>
@endsection