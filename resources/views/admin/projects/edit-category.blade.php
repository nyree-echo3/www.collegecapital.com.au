@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/projects') }}"><i class="fa fa-clipboard"></i> {{ $display_name }}</a></li>
                <li><a href="{{ url('dreamcms/projects/categories') }}">Categories</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="col-sm-12 col-md-10 col-lg-8">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Category</h3>
                    </div>

                    <form method="post" class="form-horizontal"
                          action="{{ url('dreamcms/projects/update-category') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="{{ $category->id }}">
                        <div class="box-body">
                            <div class="form-group {{ ($errors->has('name')) ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Name *</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name"
                                           value="{{ old('name',$category->name) }}">
                                    @if ($errors->has('bank_name'))
                                        <small class="help-block">{{ $errors->first('name') }}</small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ ($errors->has('slug')) ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">SEO Name *</label>
                                <div class="col-sm-10">

                                    <div class="input-group">
                                        <input type="text" id="slug" name="slug" class="form-control"
                                               value="{{ old('slug',$category->slug) }}" readonly>
                                        <span class="input-group-btn">
                                          <button type="button" class="btn btn-flat btn-info" data-toggle="modal"
                                                  data-target="#change-slug">Change SEO Name
                                          </button>
                                        </span>
                                    </div>
                                    @if ($errors->has('slug'))
                                        <small class="help-block">{{ $errors->first('slug') }}</small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ ($errors->has('description')) ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Description</label>

                                <div class="col-sm-10">
                                        <textarea id="description" name="description" rows="10" cols="80"
                                                  style="height: 500px;">{{ old('description',$category->description) }}</textarea>
                                    @if ($errors->has('description'))
                                        <small class="help-block">{{ $errors->first('description') }}</small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ ($errors->has('special_url')) ? ' has-error' : '' }}">
                                <label class="col-sm-2 control-label">Special URL</label>

                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <span class="input-group-addon">{{ env('APP_URL') }}/</span>
                                        <input type="text" name="special_url" class="form-control" value="{{ old('special_url', $category->special_url) }}">
                                    </div>
                                    @if ($errors->has('special_url'))
                                        <small class="help-block">{{ $errors->first('special_url') }}</small>
                                    @endif
                                </div>
                            </div>

                            @php
                                if(count($errors)>0){
                                   if(old('live')=='on'){
                                    $status = 'active';
                                   }else{
                                    $status = '';
                                   }
                                }else{
                                    $status = $category->status;
                                }
                            @endphp
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status *</label>
                                <div class="col-sm-10">
                                    <label>
                                        <input class="page_status" type="checkbox" data-toggle="toggle" data-size="mini"
                                               name="live" {{ $status == 'active' ? ' checked' : '' }}>
                                    </label>
                                </div>
                            </div>

                        </div>
                        <div class="box-footer">
                            <a href="{{ url('dreamcms/projects/categories') }}" class="btn btn-info pull-right">Cancel</a>
                            <button type="submit" class="btn btn-info pull-right" name="action" value="save_close">Save
                                & Close
                            </button>
                            <button type="submit" class="btn btn-info pull-right" name="action" value="save">Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </section>

    </div>

    <div class="modal fade" id="change-slug">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Change SEO Name</h4>
                </div>
                <div class="modal-body">
                    <input type="text" class="form-control" id="slug-modal" name="slug-modal"
                           value="{{ old('slug',$category->slug) }}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" id="save-seo" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            CKEDITOR.replace('description');

            $('#name').keyup(function () {
                var slug = convertToKebabCase($(this).val().toLowerCase());
                $('#slug-modal').val(slug);
                $('#slug').val(slug);
            });

            $('#slug-modal').keyup(function () {
                var slug = convertToKebabCase($(this).val().toLowerCase());
                $('#slug-modal').val(slug);
            });

            $("#save-seo").click(function () {
                var slug = $('#slug-modal').val();
                $('#slug').val(slug);
                $('#change-slug').modal('toggle');
            });
        });
    </script>
@endsection    