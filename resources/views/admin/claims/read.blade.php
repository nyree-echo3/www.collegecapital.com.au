@extends('admin/layouts/app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Claims</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-envelope"></i> Claims</a></li>
                <li class="active">Read Message</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12">

                    <div class="box box-primary">
                        <div class="box-header with-border">
                          <h3 class="box-title">Read Message</h3>

                          <div class="box-tools pull-right">
                            @if($previous)
                            <a href="{{ url('dreamcms/claims/'.$previous.'/read') }}" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Previous"><i class="fa fa-chevron-left"></i></a>
                            @endif
                            @if($next)
                            <a href="{{ url('dreamcms/claims/'.$next.'/read') }}" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Next"><i class="fa fa-chevron-right"></i></a>
                            @endif
                          </div>
                        </div>
                        <div class="box-body no-padding">
                          <div class="mailbox-read-info">
                            <h5><span class="mailbox-read-time">{{ $message->created_at }}</span></h5>
                          </div>
                          <div class="table-responsive">
                              <table class="table">
                                  @foreach(json_decode($message->data) as $field)
                                  <tr>
                                      <th style="width:20%">{{ $field->label }} :</th>
                                      <td>{{ $field->value }}</td>
                                  </tr>
                                  @endforeach
                              </table>
                          </div>
                        </div>
                        @can('delete-message')
                        <div class="box-footer">
                          <a href="{{ url('dreamcms/claims/'.$message->id.'/delete-single') }}" type="button" class="btn btn-default" data-toggle=confirmation data-title="Are you sure?" data-popout="true" data-singleton="true" data-btn-ok-label="Yes" data-btn-cancel-label="No"><i class="far fa-trash-alt"></i>Delete</a>
                        </div>
                        @endcan
                      </div> 

                </div>
            </div>
        </section>

    </div>
@endsection
@section('scripts')
  <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
        });
    </script>
@endsection