<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>DreamCMS</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('/components/theme/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/font-awesome-old2/web-fonts-with-css/css/fontawesome-all.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link href="https://fonts.googleapis.com/css?family=Great+Vibes:300,400,700" rel="stylesheet">
    
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/select2/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/theme/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/theme/dist/css/skins/_all-skins.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/toastr/toastr.min.css') }}">
    @yield('styles')
    <link rel="stylesheet" href="{{ asset('css/admin/general.css') }}">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini {!! Session::get('sidebar-state') !!}">
<div class="wrapper">
    @include('admin/partials/header')
    @include('admin/partials/menu')
    @yield('content')
    @include('admin/partials/footer')
</div>

<script src="{{ asset('/components/theme/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<script src="{{ asset('/components/theme/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/components/theme/dist/js/app.min.js') }}"></script>
<script src="{{ asset('/components/jquery-ui/jquery-ui.js') }}"></script>
<script src="{{ asset('/components/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('/components/theme/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('/components/theme/plugins/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('/components/ckfinder/ckfinder.js') }}"></script>
<script type="text/javascript">

    var base_url = '{{ url('/') }}';
    var auth_user_id = '{{ Auth::user()->id }}';

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    @if (Session::get('message'))
        $(function () {
        toastr.options = {"closeButton": true}
        toastr.{!! Session::get('message')['status'] !!}('{{Session::get('message')['text']}}')
    });
    @endif

    $(".sidebar-toggle").click(function() {
        $.ajax({
            type: "POST",
            url: "{{ url('dreamcms/sidebar-state') }}",
        });
    });

    const convertToKebabCase = (string) => {
        return string.replace(/\s+/g, '-').replace(/[^0-9a-z-]/g,"").toLowerCase();
    }

    CKEDITOR.config.customConfig = "/ckeditor/config.js";
</script>
@yield('scripts')
@yield('inline-scripts')
</body>
</html>
