@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/theme/plugins/datepicker/datepicker3.css') }}">
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{ $display_name }}</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/news') }}"><i class="fas fa-user-tie"></i> {{ $display_name }}</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-sm-12 col-md-10 col-lg-8">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit</h3>
                        </div>

                        <form method="post" class="form-horizontal" action="{{ url('dreamcms/team/update') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" value="{{ $team_member->id }}">
                            <div class="box-body">
                                <div class="form-group{{ ($errors->has('name')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Name *</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ old('name',$team_member->name) }}">
                                        @if ($errors->has('name'))
                                            <small class="help-block">{{ $errors->first('name') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('slug')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">SEO Name *</label>
                                    <div class="col-sm-10">

                                        <div class="input-group">
                                            <input type="text" id="slug" name="slug" class="form-control"
                                                   value="{{ old('slug',$team_member->slug) }}" readonly>
                                            <span class="input-group-btn">
                                              <button type="button" class="btn btn-flat btn-info" data-toggle="modal"
                                                      data-target="#change-slug">Change SEO Name
                                              </button>
                                            </span>
                                        </div>

                                        @if ($errors->has('slug'))
                                            <small class="help-block">{{ $errors->first('slug') }}</small>
                                        @endif
                                    </div>
                                </div>

                                @php
                                    if(old('category_id')!=''){
                                        $category_id = old('category_id');
                                    }else{
                                        $category_id = $team_member->category_id;
                                    }
                                @endphp
                                <div class="form-group{{ ($errors->has('category_id')) ? ' has-error' : '' }}"
                                     id="category_selector">
                                    <label class="col-sm-2 control-label">Category *</label>
                                    <div class="col-sm-10">
                                        <select name="category_id" class="form-control select2"
                                                data-placeholder="All" style="width: 100%;">
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}"{{ ($category_id == $category->id) ? ' selected="selected"' : '' }}>{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group{{ ($errors->has('job_title')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Job Title</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="job_title" placeholder="Job Title" value="{{ old('job_title',$team_member->job_title) }}">
                                        @if ($errors->has('job_title'))
                                            <small class="help-block">{{ $errors->first('job_title') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ ($errors->has('phone')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Phone</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="phone" placeholder="Phone" value="{{ old('phone', $team_member->phone) }}">
                                        @if ($errors->has('phone'))
                                            <small class="help-block">{{ $errors->first('phone') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ ($errors->has('mobile')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Mobile</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="mobile" placeholder="Mobile" value="{{ old('mobile',$team_member->mobile) }}">
                                        @if ($errors->has('mobile'))
                                            <small class="help-block">{{ $errors->first('mobile') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ ($errors->has('email')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Email</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="email" placeholder="Email" value="{{ old('email',$team_member->email) }}">
                                        @if ($errors->has('email'))
                                            <small class="help-block">{{ $errors->first('email') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ ($errors->has('role')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">LinkedIn</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="role" placeholder="LinkedIn" value="{{ old('role',$team_member->role) }}">
                                        @if ($errors->has('role'))
                                            <small class="help-block">{{ $errors->first('role') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('short_description')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Short Description *</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" rows="3" name="short_description"
                                                  placeholder="Short Description">{{ old('short_description',$team_member->short_description) }}</textarea>
                                        @if ($errors->has('short_description'))
                                            <small class="help-block">{{ $errors->first('short_description') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('body')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Body *</label>

                                    <div class="col-sm-10">
                                        <textarea id="body" name="body" rows="10" cols="80"
                                                  style="height: 500px;">{{ old('body',$team_member->body) }}</textarea>
                                        @if ($errors->has('body'))
                                            <small class="help-block">{{ $errors->first('body') }}</small>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{ ($errors->has('special_url')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Special URL</label>

                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon">{{ env('APP_URL') }}/</span>
                                            <input type="text" name="special_url" class="form-control" value="{{ old('special_url', $team_member->special_url) }}">
                                        </div>
                                        @if ($errors->has('special_url'))
                                            <small class="help-block">{{ $errors->first('special_url') }}</small>
                                        @endif
                                    </div>
                                </div>

                                @php
                                    if(count($errors)>0){
                                       if(old('photo')!=''){
                                        $photo = old('photo');
                                       }else{
                                        $photo = '';
                                       }
                                    }else{
                                        $photo = $team_member->photo;
                                    }
                                @endphp
                                <div class="form-group {{ ($errors->has('photo')) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Photo</label>
                                    <div class="col-sm-10">
                                        <input type="hidden" id="photo" name="photo"
                                               value="{{ old('photo',$photo) }}">
                                        <button id="image-popup" type="button" class="btn btn-info btn-sm">Upload
                                            Image
                                        </button>
                                        @php
                                            $class = ' invisible';
                                            if($photo!=''){
                                                $class = '';
                                            }
                                        @endphp
                                        <button id="remove-image" type="button"
                                                class="btn btn-danger btn-sm{{ $class }}">Remove Photo
                                        </button>
                                        <br/><br/>
                                        <span id="added_image">
                                        @if($photo!='')
                                                <image src="{{ old('photo',$photo) }}"/>
                                            @endif
                                        </span>
                                        @if ($errors->has('photo'))
                                            <small class="help-block">{{ $errors->first('photo') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            @php
                                if(count($errors)>0){
                                   if(old('live')=='on'){
                                    $status = 'active';
                                   }else{
                                    $status = '';
                                   }
                                }else{
                                    $status = $team_member->status;
                                }
                            @endphp
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Status * </label>
                                <div class="col-sm-10">
                                    <label>
                                        <input class="page_status" type="checkbox" data-toggle="toggle" data-size="mini"
                                               name="live" {{ $status == 'active' ? ' checked' : '' }}>
                                    </label>
                                </div>
                            </div>

                            <div class="box-footer">
                                <a href="{{ url('dreamcms/team') }}" class="btn btn-info pull-right"
                                   data-toggle=confirmation data-title="Your changes will be lost! Are you sure?"
                                   data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                   data-btn-cancel-label="No">Cancel</a>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save_close">
                                    Save & Close
                                </button>
                                <button type="submit" class="btn btn-info pull-right" name="action" value="save">Save
                                </button>

                                <br><br>
                                <a href="{{ url('dreamcms/team/'.$team_member->id.'/preview') }}" class="pull-right" target="_blank"><i class="far fa-eye"></i>&nbsp;&nbsp;Page Preview</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

    </div>

    <div class="modal fade" id="change-slug">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Change SEO Name</h4>
                </div>
                <div class="modal-body">
                    <input type="text" class="form-control" id="slug-modal" name="slug-modal"
                           value="{{ old('slug',$team_member->slug) }}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" id="save-seo" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/theme/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('/components/ckfinder/ckfinder.js') }}"></script>
    <script src="{{ asset('/components/theme/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {

            CKEDITOR.replace('body');
            CKEDITOR.replace('short_description');

            $(".select2").select2();

            $('.datepicker').datepicker({
                autoclose: true,
                format: 'dd/mm/yyyy'
            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $("#image-popup").click(function () {
                openPopup();
            });

            $("#remove-image").click(function () {
                $('#added_image').html('');
                $('#remove-image').addClass('invisible')
                $('#photo').val('');
            });

            $('#name').keyup(function () {
                var slug = convertToKebabCase($(this).val().toLowerCase());
                $('#slug-modal').val(slug);
                $('#slug').val(slug);
            });

            $('#slug-modal').keyup(function () {
                var slug = convertToKebabCase($(this).val().toLowerCase());
                $('#slug-modal').val(slug);
            });

            $("#save-seo").click(function () {
                var slug = $('#slug-modal').val();
                $('#slug').val(slug);
                $('#change-slug').modal('toggle');
            });
        });

        function openPopup() {
            CKFinder.popup({
                chooseFiles: true,
                onInit: function (finder) {
                    finder.on('files:choose', function (evt) {
                        var file = evt.data.files.first();
                        $('#added_image').html('<image src="' + base_url + file.getUrl() + '">');
                        $('#remove-image').removeClass('invisible');
                        $('#photo').val(file.getUrl());

                    });
                    finder.on('file:choose:resizedImage', function (evt) {
                        $('#added_image').html('<image src="' + base_url + evt.data.resizedUrl + '">');
                        $('#remove-image').removeClass('invisible');
                        $('#photo').val(evt.data.resizedUrl);
                    });
                }
            });
        }
    </script>
@endsection