<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Bilgin',
            'email' => 'bilgin@echo3.com.au',
            'password' => bcrypt('webwhiz@'),
            'role' => 'admin'
        ]);

        DB::table('users')->insert([
            'name' => 'Nyree',
            'email' => 'nyree@echo3.com.au',
            'password' => bcrypt('webwhiz!'),
            'role' => 'admin'
        ]);

        $this->call([
            ModulesTableSeeder::class,
            ModulesTableSeederDocument::class,
            ModulesTableSeederMember::class,
            SettingsTableSeeder::class,
			ModulesTableSeederProject::class,
			ModulesTableSeederProduct::class,
			CountriesTableSeederAustralia::class,
			ModulesTableSeederOrder::class,
			StatesTableSeeder::class,
        ]);
    }
}