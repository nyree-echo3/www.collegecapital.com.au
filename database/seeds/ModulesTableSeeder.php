<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert([
            'name' => 'Pages',
            'display_name' => 'Pages',
            'slug' => 'pages',
            'status' => 'active',
            'top_menu' => 'active',
            'position' => 0,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('modules')->insert([
            'name' => 'News',
            'display_name' => 'News',
            'slug' => 'news',
            'status' => 'active',
            'top_menu' => 'active',
            'position' => 0,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('modules')->insert([
            'name' => 'Gallery',
            'display_name' => 'Gallery',
            'slug' => 'gallery',
            'status' => 'active',
            'top_menu' => 'active',
            'position' => 0,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('modules')->insert([
            'name' => 'Contact',
            'display_name' => 'Contact',
            'slug' => 'contact',
            'status' => 'active',
            'top_menu' => 'active',
            'position' => 0,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('modules')->insert([
            'name' => 'FAQS',
            'display_name' => 'FAQS',
            'slug' => 'faqs',
            'status' => 'active',
            'top_menu' => 'active',
            'position' => 0,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);			
		
		DB::table('modules')->insert([
            'name' => 'Testimonials',
            'display_name' => 'Testimonials',
            'slug' => 'testimonials',
            'status' => 'active',
            'top_menu' => 'active',
            'position' => 0,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('modules')->insert([
            'name' => 'Team',
            'display_name' => 'Team',
            'slug' => 'team',
            'status' => 'active',
            'top_menu' => 'active',
            'position' => 0,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
		
		DB::table('modules')->insert([
            'name' => 'Claims',
            'display_name' => 'Claims',
            'slug' => 'claims',
            'status' => 'active',
            'top_menu' => 'active',
            'position' => 0,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
		
		DB::table('modules')->insert([
            'name' => 'Links',
            'display_name' => 'Links',
            'slug' => 'links',
            'status' => 'active',
            'top_menu' => 'active',
            'position' => 0,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
