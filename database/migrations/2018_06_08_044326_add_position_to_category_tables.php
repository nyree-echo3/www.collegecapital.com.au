<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPositionToCategoryTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('document_categories', function (Blueprint $table) {
            $table->integer('position')->default(1)->after('status');
        });

        Schema::table('faq_categories', function (Blueprint $table) {
            $table->integer('position')->default(1)->after('status');
        });

        Schema::table('gallery_categories', function (Blueprint $table) {
            $table->integer('position')->default(1)->after('status');
        });

        Schema::table('member_types', function (Blueprint $table) {
            $table->integer('position')->default(1)->after('status');
        });

        Schema::table('news_categories', function (Blueprint $table) {
            $table->integer('position')->default(1)->after('status');
        });

        Schema::table('project_categories', function (Blueprint $table) {
            $table->integer('position')->default(1)->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
