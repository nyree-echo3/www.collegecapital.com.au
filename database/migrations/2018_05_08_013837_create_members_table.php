<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
			$table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            $table->increments('id');
            $table->integer('type_id');
            $table->string('title');
            $table->string('firstName');
			$table->string('lastName');
			$table->string('occupation')->nullable();
			$table->string('companyName')->nullable();
			$table->string('address1');
			$table->string('address2')->nullable();
			$table->string('suburb');
			$table->string('state');
			$table->string('postcode');
			$table->string('phoneLandline')->nullable();
			$table->string('phoneMobile')->nullable();
			$table->string('email')->unique();
			$table->date('dateJoin');
			$table->date('dateExpire');
			$table->string('password');
			$table->rememberToken();
			$table->enum('newsletters', ['active','passive'])->default('passive');
            $table->enum('status', ['active','passive'])->default('passive');            
            $table->timestamps();		
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
