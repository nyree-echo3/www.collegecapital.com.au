$(document).ready(function () {

    var cms_details_error = 0;
    $('#cms-details').children('div').each(function () {
        if($(this).hasClass('has-error')){
            cms_details_error = 1;
        }
    });

    var listing_details_error = 0;
    $('#listing-details').children('div').each(function () {
        if($(this).hasClass('has-error')){
            listing_details_error = 1;
        }
    });

    var about_the_property = 0;
    $('#about-the-property').children('div').each(function () {
        if($(this).hasClass('has-error')){
            about_the_property = 1;
        }
    });

    var listing_copy_and_images = 0;
    $('#listing-copy-and-images').children('div').each(function () {
        if($(this).hasClass('has-error')){
            listing_copy_and_images = 1;
        }
    });

    var create_inspection_times = 0;
    $('#create-inspection-times').children('div').each(function () {
        if($(this).hasClass('has-error')){
            create_inspection_times = 1;
        }
    });

    if (cms_details_error==1) {
        $('a[href="#cms-details"]').tab('show');
    } else if (listing_details_error==1) {
        $('a[href="#listing-details"]').tab('show');
    } else if (about_the_property==1){
        $('a[href="#about-the-property"]').tab('show');
    }else if (listing_copy_and_images==1){
        $('a[href="#listing-copy-and-images"]').tab('show');
    }else if (create_inspection_times==1){
        $('a[href="#create-inspection-times"]').tab('show');
    }

    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass   : 'iradio_minimal-blue'
    });

    $('#price').mask("#,##0.00", {reverse: true});

    $( "#price" ).keyup(function() {
        if($( "#price" ).val()!=''){
            $("#price_text").val((toWords($("#price").val())));
        }else{
            $("#price_text").val("");
        }
    });

    CKEDITOR.replace('headline');
    CKEDITOR.replace('description');

    $(".select2").select2();

    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });

    $('.timepicker').timepicker({
        showInputs: false,
        minuteStep: 5
    });

    $('.datepicker').datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy',
        startDate: null,
    });


    var maxLengthTitle = 56;
    $('#meta_title').keyup(function () {
        var textlen = maxLengthTitle - $(this).val().length;
        $('#c_count_meta_title').text($(this).val().length + " characters | " + textlen + " characters left");
    });

    var maxLengthDescription = 250;
    $('#meta_description').keyup(function () {
        var textlen = maxLengthDescription - $(this).val().length;
        var words = $(this).val().trim().split(" ").length;
        $('#c_count_meta_description').text($(this).val().length + " characters | " + textlen + " characters left | " + words + " words");
    });

    $('#title').keyup(function () {
        var slug = convertToKebabCase($(this).val().toLowerCase());
        $('#slug-modal').val(slug);
        $('#slug').val(slug);
    });

    $('#slug-modal').keyup(function () {
        var slug = convertToKebabCase($(this).val().toLowerCase());
        $('#slug-modal').val(slug);
    });

    $("#save-seo").click(function () {
        var slug = $('#slug-modal').val();
        $('#slug').val(slug);
        $('#change-slug').modal('toggle');
    });

    $('#copy-title').click(function () {
        $('#meta_title').val($('#title').val().substr(0, maxLengthTitle));
        $('#meta_title').trigger("keyup", {which: 50});
    });

    $("#image-popup").click(function () {
        openPopup();
    });

    $("#remove-image").click(function () {
        $('#added_image').html('');
        $('#remove-image').addClass('invisible')
        $('#thumbnail').val('');
        $('#imageCount').val(0);
    });

    $("#floorplan-popup").click(function () {
        openFloorplanPopup();
    });

    $("#remove-floorplan").click(function () {
        $('#added_floorplan').html('');
        $('#remove-floorplan').addClass('invisible')
        $('#thumbnail').val('');
        $('#floorplanCount').val(0);
    });

    $("#document-popup").click(function () {
        openStatementPopup();
    });

    $("#remove-document").click(function () {
        $('#added_document').html('');
        $('#remove-document').addClass('invisible')
        $('#statement_of_information_pdf').val('');
    });

    $(".tab-next").click(function () {
        $('.nav-tabs > .active').next('li').find('a').trigger('click');
        $('html, body').animate({scrollTop:0},500);
    });

    $(".tab-prev").click(function () {
        $('.nav-tabs > .active').prev('li').find('a').trigger('click');
        $('html, body').animate({scrollTop:0},500);
    });

});