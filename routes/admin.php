<?php
Route::get('/login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);

Route::get('/dashboard', 'HomeController@dashboard');
Route::get('/', 'HomeController@dashboard');

Route::group(['prefix' => '/home'], function () {
    Route::get('', 'HomeModuleController@index');
    Route::post('update', 'HomeModuleController@update');
});

// Profile
Route::group(['prefix' => '/profile'], function () {
    Route::get('', 'ProfileController@index');
    Route::post('save-information', 'ProfileController@saveInformation');
    Route::post('change-password', 'ProfileController@changePassword');
});

// Modules
Route::group(['prefix' => '/modules', 'middleware' => 'check-module:modules'], function () {
    Route::get('', 'ModulesController@index');
    Route::post('{id}/change-status', 'ModulesController@changeStatus');
    Route::get('{id}/edit', 'ModulesController@edit');
    Route::post('update', 'ModulesController@update');
});

// Users
Route::group(['middleware' => ['permission:view-user']], function () {
    Route::group(['prefix' => '/user'], function () {
        Route::get('', 'UserController@index');
        Route::get('add', 'UserController@add')->middleware(['permission:add-user']);
        Route::post('save', 'Auth\RegisterController@register')->middleware(['permission:add-user']);
        Route::get('{id}/edit', 'UserController@edit')->middleware(['permission:edit-user']);
        Route::get('{id}/delete', 'UserController@delete')->middleware(['permission:delete-user']);
        Route::post('save-permission', 'UserController@savePermission')->middleware(['permission:edit-permission']);
        Route::post('save-password', 'UserController@savePassword')->middleware(['permission:user-change-password']);
        Route::post('save-information', 'UserController@saveInformation')->middleware(['permission:edit-user']);
        Route::post('{id}/change-user-status', 'UserController@changeUserStatus');
    });
});

// Pages Module
Route::group(['prefix' => '/pages', 'middleware' => ['check-module:pages', 'permission:view-pages']], function () {
    Route::any('', 'PagesController@index');
    Route::get('add', 'PagesController@add')->middleware(['permission:add-page']);
    Route::post('store', 'PagesController@store')->middleware(['permission:add-page']);
    Route::get('{id}/edit', 'PagesController@edit')->middleware(['permission:edit-page']);
    Route::post('update', 'PagesController@update')->middleware(['permission:edit-page']);
    Route::get('{id}/delete', 'PagesController@delete')->middleware(['permission:delete-page']);
    Route::get('{id}/preview', 'PagesController@preview');

    Route::get('categories', 'PagesController@categories');
    Route::get('add-category', 'PagesController@addCategory')->middleware(['permission:add-page-category']);
    Route::post('store-category', 'PagesController@storeCategory')->middleware(['permission:add-page-category']);
    Route::get('{id}/edit-category', 'PagesController@editCategory')->middleware(['permission:edit-page-category']);
    Route::post('update-category', 'PagesController@updateCategory')->middleware(['permission:edit-page-category']);
    Route::get('{id}/delete-category', 'PagesController@deleteCategory')->middleware(['permission:delete-page-category']);

    Route::post('{id}/change-category-status', 'PagesController@changeCategoryStatus');
    Route::post('{id}/change-page-status', 'PagesController@changePageStatus');
    Route::get('forget', 'PagesController@emptyFilter');
    Route::any('sort', 'PagesController@sort')->middleware(['permission:sort-page']);
});

// News Module
Route::group(['prefix' => '/news', 'middleware' => ['check-module:news', 'permission:view-news']], function () {
    Route::any('', 'NewsController@index');
    Route::get('add', 'NewsController@add')->middleware(['permission:add-news']);
    Route::post('store', 'NewsController@store')->middleware(['permission:add-news']);
    Route::get('{id}/edit', 'NewsController@edit')->middleware(['permission:edit-news']);
    Route::post('update', 'NewsController@update')->middleware(['permission:edit-news']);
    Route::get('{id}/delete', 'NewsController@delete')->middleware(['permission:delete-news']);
    Route::get('{id}/preview', 'NewsController@preview');

    Route::get('categories', 'NewsController@categories');
    Route::get('add-category', 'NewsController@addCategory')->middleware(['permission:add-news-category']);
    Route::post('store-category', 'NewsController@storeCategory')->middleware(['permission:add-news-category']);
    Route::get('{id}/edit-category', 'NewsController@editCategory')->middleware(['permission:edit-news-category']);
    Route::post('update-category', 'NewsController@updateCategory')->middleware(['permission:edit-news-category']);
    Route::get('{id}/delete-category', 'NewsController@deleteCategory')->middleware(['permission:delete-news-category']);
    Route::get('sort-category', 'NewsController@sortCategory')->middleware(['permission:sort-news-category']);

    Route::post('{id}/change-category-status', 'NewsController@changeCategoryStatus');
    Route::post('{id}/change-status', 'NewsController@changeStatus');
    Route::get('forget', 'NewsController@emptyFilter');
});

// FAQs Module
Route::group(['prefix' => '/faqs', 'middleware' => ['check-module:faqs', 'permission:view-faqs']], function () {
    Route::any('', 'FaqsController@index');
    Route::get('add', 'FaqsController@add')->middleware(['permission:add-faqs']);
    Route::post('store', 'FaqsController@store')->middleware(['permission:add-faqs']);
    Route::get('{id}/edit', 'FaqsController@edit')->middleware(['permission:edit-faqs']);
    Route::post('update', 'FaqsController@update')->middleware(['permission:edit-faqs']);
    Route::get('{id}/delete', 'FaqsController@delete')->middleware(['permission:delete-faqs']);
    Route::get('{id}/preview', 'FaqsController@preview');

    Route::get('categories', 'FaqsController@categories');
    Route::get('add-category', 'FaqsController@addCategory')->middleware(['permission:add-faqs-category']);
    Route::post('store-category', 'FaqsController@storeCategory')->middleware(['permission:add-faqs-category']);
    Route::get('{id}/edit-category', 'FaqsController@editCategory')->middleware(['permission:edit-faqs-category']);
    Route::post('update-category', 'FaqsController@updateCategory')->middleware(['permission:edit-faqs-category']);
    Route::get('{id}/delete-category', 'FaqsController@deleteCategory')->middleware(['permission:delete-faqs-category']);
    Route::get('sort-category', 'FaqsController@sortCategory')->middleware(['permission:sort-faqs-category']);

    Route::post('{id}/change-faq-status', 'FaqsController@changeFaqStatus');
    Route::post('{id}/change-category-status', 'FaqsController@changeCategoryStatus');
    Route::get('forget', 'FaqsController@emptyFilter');
    Route::get('sort', 'FaqsController@sort')->middleware(['permission:sort-faq']);
});

// Gallery Module
Route::group(['prefix' => '/gallery', 'middleware' => ['check-module:gallery', 'permission:view-gallery']], function () {
    Route::any('', 'GalleryController@index');
    Route::get('add', 'GalleryController@add')->middleware(['permission:add-image']);
    Route::post('store', 'GalleryController@store')->middleware(['permission:add-image']);
    Route::get('{id}/edit', 'GalleryController@edit')->middleware(['permission:edit-image']);
    Route::post('update', 'GalleryController@update')->middleware(['permission:edit-image']);
    Route::get('{id}/delete', 'GalleryController@delete')->middleware(['permission:delete-image']);
    Route::get('{id}/show', 'GalleryController@show');

    Route::get('categories', 'GalleryController@categories');
    Route::get('add-category', 'GalleryController@addCategory')->middleware(['permission:add-gallery-category']);
    Route::post('store-category', 'GalleryController@storeCategory')->middleware(['permission:add-gallery-category']);
    Route::get('{id}/edit-category', 'GalleryController@editCategory')->middleware(['permission:edit-gallery-category']);
    Route::post('update-category', 'GalleryController@updateCategory')->middleware(['permission:edit-gallery-category']);
    Route::get('{id}/delete-category', 'GalleryController@deleteCategory')->middleware(['permission:delete-gallery-category']);
    Route::get('sort-category', 'GalleryController@sortCategory')->middleware(['permission:sort-gallery-category']);

    Route::post('{id}/change-category-status', 'GalleryController@changeCategoryStatus');
    Route::post('{id}/change-status', 'GalleryController@changeStatus')->middleware(['permission:edit-image']);
    Route::post('image-sort', 'GalleryController@imageSort')->middleware(['permission:edit-image']);
    Route::get('forget', 'GalleryController@emptyFilter');
});

// Members Module
Route::group(['prefix' => '/members', 'middleware' => ['check-module:members', 'permission:view-members']], function () {
    Route::any('', 'MembersController@index');
    Route::get('add', 'MembersController@add')->middleware(['permission:add-members']);
    Route::post('store', 'MembersController@store')->middleware(['permission:add-members']);
    Route::get('{id}/edit', 'MembersController@edit')->middleware(['permission:edit-members']);
    Route::post('update', 'MembersController@update')->middleware(['permission:edit-members']);
    Route::get('{id}/delete', 'MembersController@delete')->middleware(['permission:delete-members']);
    Route::get('{id}/show', 'MembersController@show');

    Route::get('types', 'MembersController@types');
    Route::get('add-type', 'MembersController@addType')->middleware(['permission:add-members-type']);
    Route::post('store-type', 'MembersController@storeType')->middleware(['permission:add-members-type']);
    Route::get('{id}/edit-type', 'MembersController@editType')->middleware(['permission:edit-members-type']);
    Route::post('update-type', 'MembersController@updateType')->middleware(['permission:edit-members-type']);
    Route::get('{id}/delete-type', 'MembersController@deleteType')->middleware(['permission:delete-members-type']);
    Route::get('sort-type', 'MembersController@sortType')->middleware(['permission:sort-members-type']);

    Route::post('{id}/change-member-status', 'MembersController@changeMemberStatus');
    Route::post('{id}/change-type-status', 'MembersController@changeTypeStatus');
    Route::get('forget', 'MembersController@emptyFilter');
    Route::get('sort', 'MembersController@sort')->middleware(['permission:sort-member']);
});

// Documents Module
Route::group(['prefix' => '/documents', 'middleware' => ['check-module:documents', 'permission:view-documents']], function () {
    Route::any('', 'DocumentsController@index');
    Route::get('add', 'DocumentsController@add')->middleware(['permission:add-documents']);
    Route::post('store', 'DocumentsController@store')->middleware(['permission:add-documents']);
    Route::get('{id}/edit', 'DocumentsController@edit')->middleware(['permission:edit-documents']);
    Route::post('update', 'DocumentsController@update')->middleware(['permission:edit-documents']);
    Route::get('{id}/delete', 'DocumentsController@delete')->middleware(['permission:delete-documents']);
    Route::get('{id}/preview', 'DocumentsController@preview');

    Route::get('categories', 'DocumentsController@categories');
    Route::get('add-category', 'DocumentsController@addCategory')->middleware(['permission:add-documents-category']);
    Route::post('store-category', 'DocumentsController@storeCategory')->middleware(['permission:add-documents-category']);
    Route::get('{id}/edit-category', 'DocumentsController@editCategory')->middleware(['permission:edit-documents-category']);
    Route::post('update-category', 'DocumentsController@updateCategory')->middleware(['permission:edit-documents-category']);
    Route::get('{id}/delete-category', 'DocumentsController@deleteCategory')->middleware(['permission:delete-documents-category']);
    Route::get('sort-category', 'DocumentsController@sortCategory')->middleware(['permission:sort-documents-category']);

    Route::post('{id}/change-document-status', 'DocumentsController@changeDocumentStatus');
    Route::post('{id}/change-category-status', 'DocumentsController@changeCategoryStatus');
    Route::get('forget', 'DocumentsController@emptyFilter');
    Route::get('sort', 'DocumentsController@sort')->middleware(['permission:sort-documents']);
});

// Contact Module
Route::group(['prefix' => '/contact', 'middleware' => ['check-module:contact', 'permission:view-contact']], function () {
    Route::any('', 'ContactController@index');
    Route::get('details', 'ContactController@details')->middleware(['permission:view-edit-details']);
    Route::get('form-builder', 'ContactController@formBuilder')->middleware(['permission:view-edit-details']);
    Route::post('get-form', 'ContactController@getForm')->middleware(['permission:view-contact']);
    Route::post('save-form', 'ContactController@saveForm')->middleware(['permission:view-edit-details']);
    Route::post('store', 'ContactController@store')->middleware(['permission:view-edit-details']);
    Route::post('delete', 'ContactController@delete')->middleware(['permission:delete-message']);
    Route::get('{id}/delete-single', 'ContactController@deleteSingle')->middleware(['permission:delete-message']);
    Route::post('{id}/change-favourite', 'ContactController@changeFavouriteStatus');
    Route::get('{id}/read', 'ContactController@read')->middleware(['permission:read-message']);
});

// Claims Module
Route::group(['prefix' => '/claims', 'middleware' => ['check-module:claims', 'permission:view-claims']], function () {
    Route::any('', 'ClaimsController@index');
    Route::get('details', 'ClaimsController@details')->middleware(['permission:view-edit-details']);
	
    Route::get('claims-form-builder', 'ClaimsController@claimsFormBuilder')->middleware(['permission:view-edit-details']);	
    Route::post('get-claims-form', 'ClaimsController@getClaimsForm')->middleware(['permission:view-contact']);
    Route::post('save-claims-form', 'ClaimsController@saveClaimsForm')->middleware(['permission:view-edit-details']);
	
	Route::get('quick-claims-form-builder', 'ClaimsController@quickClaimsFormBuilder')->middleware(['permission:view-edit-details']);
    Route::post('get-quick-claims-form', 'ClaimsController@getQuickClaimsForm')->middleware(['permission:view-contact']);
    Route::post('save-quick-claims-form', 'ClaimsController@saveQuickClaimsForm')->middleware(['permission:view-edit-details']);
	
	Route::post('store', 'ClaimsController@store')->middleware(['permission:view-edit-details']);
    Route::post('delete', 'ClaimsController@delete')->middleware(['permission:delete-message']);
    Route::get('{id}/delete-single', 'ClaimsController@deleteSingle')->middleware(['permission:delete-message']);
    Route::post('{id}/change-favourite', 'ClaimsController@changeFavouriteStatus');
    Route::get('{id}/read', 'ClaimsController@read')->middleware(['permission:read-message']);
});

// Products Module
Route::group(['prefix' => '/products', 'middleware' => ['check-module:products', 'permission:view-products']], function () {
    Route::any('', 'ProductsController@index');
    Route::get('add', 'ProductsController@add')->middleware(['permission:add-products']);
    Route::post('store', 'ProductsController@store')->middleware(['permission:add-products']);
    Route::get('{id}/edit', 'ProductsController@edit')->middleware(['permission:edit-products']);
    Route::post('update', 'ProductsController@update')->middleware(['permission:edit-products']);
    Route::get('{id}/delete', 'ProductsController@delete')->middleware(['permission:delete-products']);
    Route::get('{id}/show', 'ProductsController@show');
	Route::get('{id}/preview', 'ProductsController@preview');

    Route::get('categories', 'ProductsController@categories');
    Route::get('add-category', 'ProductsController@addCategory')->middleware(['permission:add-products-category']);
    Route::post('store-category', 'ProductsController@storeCategory')->middleware(['permission:add-products-category']);
    Route::get('{id}/edit-category', 'ProductsController@editCategory')->middleware(['permission:edit-products-category']);
    Route::post('update-category', 'ProductsController@updateCategory')->middleware(['permission:edit-products-category']);
    Route::get('{id}/delete-category', 'ProductsController@deleteCategory')->middleware(['permission:delete-products-category']);
    Route::get('sort-category', 'ProductsController@sortCategory')->middleware(['permission:sort-products-category']);
	
    Route::post('{id}/change-product-status', 'ProductsController@changeProductStatus');
    Route::post('{id}/change-category-status', 'ProductsController@changeCategoryStatus');
    Route::get('forget', 'ProductsController@emptyFilter');
    Route::get('sort', 'ProductsController@sort')->middleware(['permission:sort-products']);
});

// Orders Module
Route::group(['prefix' => '/orders', 'middleware' => ['check-module:orders', 'permission:view-orders']], function () {
    Route::any('', 'OrdersController@index');   
    Route::get('{id}/edit', 'OrdersController@edit')->middleware(['permission:edit-orders']);
    Route::post('update', 'OrdersController@update')->middleware(['permission:edit-orders']);
    Route::get('{id}/delete', 'OrdersController@delete')->middleware(['permission:delete-orders']); 	   
    Route::get('forget', 'OrdersController@emptyFilter');   
});

// Projects Module
Route::group(['prefix' => '/projects', 'middleware' => ['check-module:projects', 'permission:view-projects']], function () {
    Route::any('', 'ProjectsController@index');
    Route::get('add', 'ProjectsController@add')->middleware(['permission:add-projects']);
    Route::post('store', 'ProjectsController@store')->middleware(['permission:add-projects']);
    Route::get('{id}/edit', 'ProjectsController@edit')->middleware(['permission:edit-projects']);
    Route::post('update', 'ProjectsController@update')->middleware(['permission:edit-projects']);
    Route::get('{id}/delete', 'ProjectsController@delete')->middleware(['permission:delete-projects']);
    Route::get('{id}/show', 'ProjectsController@show');

    Route::get('categories', 'ProjectsController@categories');
    Route::get('add-category', 'ProjectsController@addCategory')->middleware(['permission:add-projects-category']);
    Route::post('store-category', 'ProjectsController@storeCategory')->middleware(['permission:add-projects-category']);
    Route::get('{id}/edit-category', 'ProjectsController@editCategory')->middleware(['permission:edit-projects-category']);
    Route::post('update-category', 'ProjectsController@updateCategory')->middleware(['permission:edit-projects-category']);
    Route::get('{id}/delete-category', 'ProjectsController@deleteCategory')->middleware(['permission:delete-projects-category']);

    Route::post('{id}/change-project-status', 'ProjectsController@changeProjectStatus');
    Route::post('{id}/change-category-status', 'ProjectsController@changeCategoryStatus');
    Route::get('forget', 'ProjectsController@emptyFilter');
    Route::get('sort', 'ProjectsController@sort')->middleware(['permission:sort-projects']);
});

// Properties Module
Route::group(['prefix' => '/properties', 'middleware' => ['check-module:properties', 'permission:view-properties']], function () {
    Route::any('', 'PropertiesController@index');
    Route::get('add', 'PropertiesController@add')->middleware(['permission:add-properties']);
    Route::post('store', 'PropertiesController@store')->middleware(['permission:add-properties']);
    Route::get('{id}/edit', 'PropertiesController@edit')->middleware(['permission:edit-properties']);
    Route::post('update', 'PropertiesController@update')->middleware(['permission:edit-properties']);
    Route::get('{id}/delete', 'PropertiesController@delete')->middleware(['permission:delete-properties']);
    Route::get('{id}/show', 'PropertiesController@show');

    Route::get('categories', 'PropertiesController@categories');
    Route::get('add-category', 'PropertiesController@addCategory')->middleware(['permission:add-properties-category']);
    Route::post('store-category', 'PropertiesController@storeCategory')->middleware(['permission:add-properties-category']);
    Route::get('{id}/edit-category', 'PropertiesController@editCategory')->middleware(['permission:edit-properties-category']);
    Route::post('update-category', 'PropertiesController@updateCategory')->middleware(['permission:edit-properties-category']);
    Route::get('{id}/delete-category', 'PropertiesController@deleteCategory')->middleware(['permission:delete-properties-category']);
    Route::get('sort-category', 'PropertiesController@sortCategory')->middleware(['permission:sort-team-category']);

    Route::post('{id}/change-property-status', 'PropertiesController@changePropertyStatus');
    Route::post('{id}/change-category-status', 'PropertiesController@changeCategoryStatus');
    Route::get('forget', 'PropertiesController@emptyFilter');
    Route::get('sort', 'PropertiesController@sort')->middleware(['permission:sort-properties']);
});

// Testimonials Module
Route::group(['prefix' => '/testimonials', 'middleware' => ['check-module:testimonials', 'permission:view-testimonials']], function () {
    Route::any('', 'TestimonialsController@index');
    Route::get('add', 'TestimonialsController@add')->middleware(['permission:add-testimonials']);
    Route::post('store', 'TestimonialsController@store')->middleware(['permission:add-testimonials']);
    Route::get('{id}/edit', 'TestimonialsController@edit')->middleware(['permission:edit-testimonials']);
    Route::post('update', 'TestimonialsController@update')->middleware(['permission:edit-testimonials']);
    Route::get('{id}/delete', 'TestimonialsController@delete')->middleware(['permission:delete-testimonials']);
    Route::get('{id}/preview', 'TestimonialsController@preview');

    Route::post('{id}/change-testimonial-status', 'TestimonialsController@changeTestimonialStatus');
    Route::get('forget', 'TestimonialsController@emptyFilter');
    Route::get('sort', 'TestimonialsController@sort')->middleware(['permission:sort-testimonial']);
});

// Team Module
Route::group(['prefix' => '/team', 'middleware' => ['check-module:team', 'permission:view-team']], function () {
    Route::any('', 'TeamController@index');
    Route::get('add', 'TeamController@add')->middleware(['permission:add-team-member']);
    Route::post('store', 'TeamController@store')->middleware(['permission:add-team-member']);
    Route::get('{id}/edit', 'TeamController@edit')->middleware(['permission:edit-team-member']);
    Route::post('update', 'TeamController@update')->middleware(['permission:edit-team-member']);
    Route::get('{id}/delete', 'TeamController@delete')->middleware(['permission:delete-team-member']);
    Route::get('{id}/preview', 'TeamController@preview');

    Route::get('categories', 'TeamController@categories');
    Route::get('add-category', 'TeamController@addCategory')->middleware(['permission:add-team-category']);
    Route::post('store-category', 'TeamController@storeCategory')->middleware(['permission:add-team-category']);
    Route::get('{id}/edit-category', 'TeamController@editCategory')->middleware(['permission:edit-team-category']);
    Route::post('update-category', 'TeamController@updateCategory')->middleware(['permission:edit-team-category']);
    Route::get('{id}/delete-category', 'TeamController@deleteCategory')->middleware(['permission:delete-team-category']);
    Route::get('sort-category', 'TeamController@sortCategory')->middleware(['permission:sort-team-category']);

    Route::post('{id}/change-member-status', 'TeamController@changeMemberStatus');
    Route::post('{id}/change-category-status', 'TeamController@changeCategoryStatus');
    Route::get('forget', 'TeamController@emptyFilter');
    Route::get('sort', 'TeamController@sort')->middleware(['permission:sort-team-member']);
});

// Links Module
Route::group(['prefix' => '/links', 'middleware' => ['check-module:links', 'permission:view-links']], function () {
    Route::any('', 'LinksController@index');
    Route::get('add', 'LinksController@add')->middleware(['permission:add-link']);
    Route::post('store', 'LinksController@store')->middleware(['permission:add-link']);
    Route::get('{id}/edit', 'LinksController@edit')->middleware(['permission:edit-link']);
    Route::post('update', 'LinksController@update')->middleware(['permission:edit-link']);
    Route::get('{id}/delete', 'LinksController@delete')->middleware(['permission:delete-link']);

    Route::get('categories', 'LinksController@categories');
    Route::get('add-category', 'LinksController@addCategory')->middleware(['permission:add-links-category']);
    Route::post('store-category', 'LinksController@storeCategory')->middleware(['permission:add-links-category']);
    Route::get('{id}/edit-category', 'LinksController@editCategory')->middleware(['permission:edit-links-category']);
    Route::post('update-category', 'LinksController@updateCategory')->middleware(['permission:edit-links-category']);
    Route::get('{id}/delete-category', 'LinksController@deleteCategory')->middleware(['permission:delete-links-category']);

    Route::post('{id}/change-category-status', 'LinksController@changeCategoryStatus');
    Route::post('{id}/change-link-status', 'LinksController@changeLinkStatus');
    Route::get('forget', 'LinksController@emptyFilter');
    Route::any('sort', 'LinksController@sort')->middleware(['permission:sort-link']);
});

// Settings Module
Route::group(['middleware' => ['permission:view-settings']], function () {
    Route::group(['prefix' => '/settings'], function () {
        Route::get('', 'SettingsController@index')->middleware(['permission:edit-general']);
        Route::post('update', 'SettingsController@update')->middleware(['permission:edit-general']);

        Route::get('home-page', 'SettingsController@homePage')->middleware(['permission:edit-home-page']);
        Route::post('home-page-update', 'SettingsController@homePageUpdate')->middleware(['permission:edit-home-page']);

        Route::get('header-images', 'SettingsController@headerImages')->middleware(['permission:edit-header-images']);
        Route::post('save-header-image', 'SettingsController@saveHeaderImage')->middleware(['permission:edit-header-images']);
        Route::post('remove-header-image', 'SettingsController@removeHeaderImage')->middleware(['permission:edit-header-images']);

        Route::get('contacts', 'SettingsController@contacts')->middleware(['permission:edit-contact-details']);
        Route::post('update-contacts', 'SettingsController@contactsUpdate')->middleware(['permission:edit-contact-details']);

        Route::get('navigation', 'SettingsController@navigation')->middleware(['permission:edit-navigation']);
        Route::post('{id}/change-status', 'SettingsController@changeStatus')->middleware(['permission:edit-navigation']);
        Route::post('{id}/change-tm-status', 'SettingsController@changeTopMenuStatus')->middleware(['permission:edit-navigation']);
        Route::post('navigation-sort', 'SettingsController@navigationSort')->middleware(['permission:edit-navigation']);

        Route::get('home-sliders', 'SettingsController@homeSliders')->middleware(['permission:edit-home-page-slider']);
        Route::get('add-home-slider', 'SettingsController@homeSlidersAdd')->middleware(['permission:edit-home-page-slider']);
        Route::post('store-home-slider', 'SettingsController@homeSlidersStore')->middleware(['permission:edit-home-page-slider']);
        Route::get('{id}/edit-home-slider', 'SettingsController@homeSlidersEdit')->middleware(['permission:edit-home-page-slider']);
        Route::get('{id}/delete-home-slider', 'SettingsController@homeSlidersDelete')->middleware(['permission:delete-home-page-slider']);
        Route::post('update-home-slider', 'SettingsController@homeSlidersUpdate')->middleware(['permission:edit-home-page-slider']);
        Route::post('{id}/change-status-home-slider', 'SettingsController@homeSlidersChangeStatus')->middleware(['permission:edit-home-page-slider']);
        Route::post('image-sort-home-slider', 'SettingsController@homeSlidersImageSort')->middleware(['permission:edit-home-page-slider']);

        Route::get('social-media', 'SettingsController@socialMedia')->middleware(['permission:edit-social-media']);
        Route::post('social-media-update', 'SettingsController@socialMediaUpdate')->middleware(['permission:edit-social-media']);
    });
});

Route::post('sort', '\Rutorika\Sortable\SortableController@sort');
Route::get('media-library', 'MediaLibraryController@index');
Route::post('sidebar-state', 'HomeController@sidebarState');
Route::post('pagination', 'HomeController@pagination');